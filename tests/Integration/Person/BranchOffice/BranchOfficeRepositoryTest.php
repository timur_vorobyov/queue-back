<?php

namespace Tests\Integration\Person\BranchOffice;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\BranchOffice\Person\BranchOfficeRepository;
use App\Modules\BranchOffice\Person\Interfaces\IBranchOfficeRepository;
use App\Modules\Town\Town;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BranchOfficeRepositoryTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private IBranchOfficeRepository $branchOfficeRepository;
    private FactoryObject $branchOfficeFactory, $townFactory;
    protected function setUp(): void
    {
        parent::setUp();
        $this->branchOfficeRepository = app(BranchOfficeRepository::class);
        $this->townFactory = Town::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
    }

    public function testGetBranchOfficesFilteredByTownId()
    {
        $this->withoutExceptionHandling();

        $townId = $this->townFactory->create()->id;

        $branchOffices = $this->branchOfficeFactory
            ->count(4)
            ->create(["town_id" => $townId])
            ->map(function ($branchOffice) {
                return $branchOffice->toDomainEntity()->jsonSerialize();
            });

        $receivedBranchOffices = $this->branchOfficeRepository->getBranchOfficesFilteredByTownId(
            $townId,
        );

        $this->assertEquals($receivedBranchOffices, $branchOffices);
    }
}
