<?php

namespace Tests\Integration\Person\Ticket;

use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use App\Modules\Ticket\Person\Jobs\SmsSenderJob;
use App\Modules\Ticket\Person\TicketRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class TicketEventsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private ITicketRepository $ticketRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->ticketRepository = app(TicketRepository::class);
    }

    public function testSmsSenderJobIsPushedToQueue()
    {
        Queue::fake();

        SmsSenderJob::dispatch("900201200", "Номер вашей очереди");

        Queue::assertPushed(SmsSenderJob::class);
    }

    public function testSmsSenderJobIsDispatched()
    {
        Bus::fake();

        SmsSenderJob::dispatch("900201200", "Номер вашей очереди");

        Bus::assertDispatched(SmsSenderJob::class);
    }
}
