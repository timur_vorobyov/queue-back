<?php

namespace Tests\Integration\Person\Ticket;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use App\Modules\Ticket\Ticket;
use App\Modules\Ticket\Person\TicketRepository;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;

class TicketRepositoryTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private Ticket $ticket;
    private ITicketRepository $ticketRepository;
    private FactoryObject $ticketFactory,
        $userFactory,
        $visitPurposeFactory,
        $statusFactory,
        $branchOfficeFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->ticketRepository = app(TicketRepository::class);
        $this->ticket = app(Ticket::class);
        $this->ticketFactory = Ticket::factory();
        $this->userFactory = User::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->statusFactory = Status::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
    }

    public function testGetFullTicketsList()
    {
        $this->withoutExceptionHandling();

        $branchOffice = $this->branchOfficeFactory->create();
        $branchOfficeId = $branchOffice->id;
        $visitPurpose = $this->visitPurposeFactory
            ->hasAttached($branchOffice)
            ->create(['id' => 3]);
        $status = $this->statusFactory->create(["name" => "new"]);
        $userId = $this->userFactory
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => $visitPurpose->id])
            ->create([
            "branch_office_id" => $branchOfficeId
        ])->id;

        $this->ticketFactory
            ->count(4)
            ->create([
                "branch_office_id" => $branchOfficeId,
                "visit_purpose_id" => $visitPurpose->id,
                "status_id" => $status->id,
                "user_id" => $userId,
            ]);

        $tickets = $this->ticket
            ::with("visitPurpose", "branchOffice")
            ->where("branch_office_id", $branchOfficeId)
            ->whereIn("visit_purpose_id", [$visitPurpose->id])
            ->where("status_id", $status->id)
            ->get()
            ->map(function ($ticket) {
                return $ticket->toDomainEntity()->jsonSerialize();
            });

        $receivedTickets = $this->ticketRepository->getFullTicketsList(
            $branchOfficeId,
            $status->name,
            [$visitPurpose->id],
            $userId,
        );

        $this->assertEquals($receivedTickets, $tickets);
    }

    public function testGetShortTicketsList()
    {
        $this->withoutExceptionHandling();

        $branchOffice = $this->branchOfficeFactory->create();
        $branchOfficeId = $branchOffice->id;
        $visitPurposeId = $this->visitPurposeFactory
            ->hasAttached($branchOffice)
            ->create(['id' => 2])->id;
        $status = $this->statusFactory->create(["name" => "new"]);
        $userId = $this->userFactory
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => $visitPurposeId])
            ->create([
            "branch_office_id" => $branchOfficeId,
        ])->id;

        $this->ticketFactory
            ->count(4)
            ->create([
                "branch_office_id" => $branchOfficeId,
                "visit_purpose_id" => $visitPurposeId,
                "status_id" => $status->id,
                "user_id" => $userId,
            ]);

        $tickets = $this->ticket
            ::with("user", "visitPurpose", "branchOffice")
            ->where("branch_office_id", $branchOfficeId)
            ->where("status_id", $status->id)
            ->get()
            ->map(function ($ticket) {
                return $ticket->toDomainEntity()->jsonSerializeShortTicket();
            });

        $receivedTickets = $this->ticketRepository->getShortTicketsList(
            $branchOfficeId,
            $status->name,
            [$visitPurposeId]
        );

        $this->assertEquals($receivedTickets, $tickets);
    }

    public function testGetUserVisitPurposeCount()
    {
        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create();

        $userVisitPurposesCount = $this->ticketRepository->getUserVisitPurposesCount(
            $user->id,
        );
        $this->assertEquals(1, $userVisitPurposesCount);
    }

    public function testIsUserDeskNumberNull()
    {
        $this->ticketFactory->create();
        $userId = $this->userFactory->create()->id;
        $isFalse = $this->ticketRepository->isUserDeskNumberNull($userId);

        $this->assertEquals(false, $isFalse);
    }

    public function testGetTicketFilteredById()
    {
        $this->statusFactory->create(["id" => 1, "name" => "new"]);
        $visitPurpose = $this->visitPurposeFactory->create();
        $user = $this->userFactory->create();
        $ticket = $this->ticketFactory->create([
            "status_id" => 1,
            "user_id" => $user->id,
            "visit_purpose_id" => $visitPurpose->id,
            "branch_office_id" => $user->branch_office_id,
            "invited_at" => Carbon::now(),
        ]);

        $ticket = $this->ticketRepository->getTicketFilteredById(
            $ticket->id
        );
        $this->assertObjectHasAttribute("visitPurposeId", $ticket);
        $this->assertObjectHasAttribute("customerPhoneNumber", $ticket);
        $this->assertObjectHasAttribute("branchOfficeId", $ticket);
        $this->assertObjectHasAttribute("ticketNumber", $ticket);
        $this->assertObjectHasAttribute("statusId", $ticket);
        $this->assertObjectHasAttribute("serviceId", $ticket);
        $this->assertObjectHasAttribute("userId", $ticket);
        $this->assertObjectHasAttribute("invitedAt", $ticket);
        $this->assertObjectHasAttribute("visitPurpose", $ticket);
        $this->assertObjectHasAttribute("user", $ticket);
        $this->assertObjectHasAttribute("branchOffice", $ticket);
    }
}
