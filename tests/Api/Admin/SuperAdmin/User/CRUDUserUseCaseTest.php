<?php

namespace Tests\Api\Admin\SuperAdmin\User;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Role\Role;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CRUDUserUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $userFactory,
        $branchOfficeFactory,
        $visitPurposeFactory,
        $roleFactory;
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userFactory = User::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->roleFactory = Role::factory();
        $this->user = app(User::class);
    }

    public function testASuperAdminCanCreateUser()
    {
        $this->withoutExceptionHandling();
        $visitPurposesIds = $this->visitPurposeFactory
            ->count(2)
            ->create()
            ->map(function ($visitPurpose) {
                return $visitPurpose->id;
            })
            ->toArray();

        $attributes = [
            "branchOfficeId" => $this->branchOfficeFactory->create()->id,
            "visitPurposesIds" => $visitPurposesIds,
            "firstName" => $this->faker->firstName,
            "lastName" => $this->faker->lastName,
            "email" => $this->faker->unique()->safeEmail,
            "password" => "123456789",
            "isActive" => true,
            "isAllowedHaveSameDeskNumber" => true,
            "roleId" => $this->roleFactory->create()->id,
        ];

        $this->post(route("super-admin.storeUser", $attributes))->assertStatus(
            200,
        );

        foreach ($visitPurposesIds as $visitPurposeId) {
            $this->assertDatabaseHas("user_visit_purpose", [
                "visit_purpose_id" => $visitPurposeId,
            ]);
        }

        $requestStub = new CRUDUserRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();

        $this->assertDatabaseHas("users", $modifiedAttributes);
    }

    public function testASuperAdminCanUpdateUser()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])
            ->create([
            "branch_office_id" => $this->branchOfficeFactory->create(["id" => 1])->id,
            "first_name" => "Alex",
            "last_name" => "Ran",
            "email" => "alex@gmail.com",
            "password" => "123456789",
            "is_active" => false,
        ]);

        $visitPurposesIds = $this->visitPurposeFactory
            ->count(3)
            ->create()
            ->pluck("id")
            ->all();

        $attributes = [
            "id" => $user->id,
            "branchOfficeId" => $this->branchOfficeFactory->create(["id" => 5])
                ->id,
            "firstName" => $this->faker->firstName,
            "lastName" => $this->faker->lastName,
            "email" => $this->faker->unique()->safeEmail,
            "isActive" => true,
            "roleId" => $this->roleFactory->create(["id" => 3])->id,
            "visitPurposesIds" => $visitPurposesIds,
            "isAllowedHaveSameDeskNumber" => true,
        ];

        $response = $this->put(
            "/api/super-admin/users/" . $user->id,
            $attributes,
        )->assertStatus(200);

        $updatedUser = $this->user
            ::find($user->id)
            ->toDomainEntity()
            ->superAdminJsonSerialize();
        array_splice($updatedUser, count($updatedUser) - 7);
        array_splice($attributes, count($attributes) - 3);
        $this->assertEquals($updatedUser, $attributes);
        $this->assertEquals("User is updated", $response["message"]);

        $pivotTablesIds = DB::table("user_visit_purpose")
            ->whereIn("visit_purpose_id", $visitPurposesIds)
            ->pluck("id")
            ->all();

        foreach ($pivotTablesIds as $pivotTableId) {
            $this->assertDatabaseHas("user_visit_purpose", [
                "id" => $pivotTableId,
            ]);
        }
    }

    public function testASuperAdminCanDeleteUser()
    {
        $user = $this->userFactory->create();
        $this->delete("/api/super-admin/user/" . $user->id);
        $this->assertDatabaseMissing("users", ["id" => $user->id]);
    }

    public function testASuperAdminCanGetPaginatedUsersListFilteredBySearchExpresion()
    {
        $limit = 5;
        $currentPage = 1;
        $totalPages = 2;
        $totalRecords = 6;

        $this->userFactory->count(2)->create([
            "first_name" => "Alex",
        ]);

        $this->userFactory->count(6)->create([
            "first_name" => "John",
        ]);

        $attributes = [
            "searchExpression" => "John",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedUsersList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetPaginatedAllUsersList()
    {
        $this->userFactory->count(5)->create([
            "first_name" => "Alex",
        ]);

        $this->userFactory->count(8)->create([
            "first_name" => "John",
        ]);

        $limit = 5;
        $currentPage = 1;
        $totalPages = 3;
        $totalRecords = 13;

        $attributes = [
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedUsersList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetPaginatedUsersListFilteredByBranchOffice()
    {
        $this->userFactory->count(5)->create([
            "branch_office_id" => "1",
        ]);

        $this->userFactory->count(8)->create([
            "branch_office_id" => "2",
        ]);

        $limit = 5;
        $currentPage = 2;
        $totalPages = 2;
        $totalRecords = 8;

        $attributes = [
            "branchOfficeId" => "2",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedUsersList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetPaginatedUsersListFilteredByBranchOfficeAndSearchExpression()
    {
        $this->userFactory->count(2)->create([
            "branch_office_id" => "1",
            "first_name" => "Alex",
        ]);

        $this->userFactory->count(3)->create([
            "branch_office_id" => "1",
            "first_name" => "John",
        ]);

        $this->userFactory->count(3)->create([
            "branch_office_id" => "2",
            "first_name" => "Marcel",
        ]);

        $limit = 5;
        $currentPage = 1;
        $totalPages = 1;
        $totalRecords = 2;

        $attributes = [
            "branchOfficeId" => "1",
            "searchExpression" => "Alex",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getPaginatedUsersList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }
}
