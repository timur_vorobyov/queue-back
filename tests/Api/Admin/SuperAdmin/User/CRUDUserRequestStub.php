<?php

namespace Tests\Api\Admin\SuperAdmin\User;

class CRUDUserRequestStub
{
    private array $storeActionData;
    public function __construct(array $storeActionData)
    {
        $this->storeActionData = $storeActionData;
    }

    public function toDatabaseFormat(): array
    {
        return [
            "branch_office_id" => $this->storeActionData["branchOfficeId"],
            "first_name" => $this->storeActionData["firstName"],
            "last_name" => $this->storeActionData["lastName"],
            "email" => $this->storeActionData["email"],
            "is_active" => $this->storeActionData["isActive"],
        ];
    }
}
