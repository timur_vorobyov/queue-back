<?php

namespace Tests\Api\Admin\SuperAdmin\Service;

class CRUDServiceRequestStub
{
    private array $storeActionData;
    public function __construct(array $storeActionData)
    {
        $this->storeActionData = $storeActionData;
    }

    public function toDatabaseFormat(): array
    {
        return [
            "name" => $this->storeActionData["name"],
            "is_active" => $this->storeActionData["isActive"],
        ];
    }
}
