<?php

namespace Tests\Api\Admin\SuperAdmin\Service;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Service\Service;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CRUDServiceUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $serviceFactory, $visitPurposeFactory;
    private Service $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serviceFactory = Service::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->service = app(Service::class);
    }

    public function testASuperAdminCanCreateService()
    {
        $this->withoutExceptionHandling();

        $attributes = [
            "name" => $this->faker->word,
            "isActive" => true,
        ];

        $requestStub = new CRUDServiceRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();

        $this->post(
            route("super-admin.storeService", $attributes),
        )->assertStatus(200);
        $this->assertDatabaseHas("services", $modifiedAttributes);
    }

    public function testASuperAdminCanUpdateService()
    {
        $this->withoutExceptionHandling();

        $service = $this->serviceFactory->create();

        $attributes = [
            "id" => $service->id,
            "name" => "run",
            "isActive" => false,
        ];

        $response = $this->put(
            "/api/super-admin/services/" . $service->id,
            $attributes,
        )->assertStatus(200);

        $updatedService = $this->service
            ::find($service->id)
            ->toDomainEntity()
            ->jsonSerialize();

        $this->assertEquals($updatedService, $attributes);
        $this->assertEquals("Service is updated", $response["message"]);
    }

    public function testASuperAdminCanDeleteService()
    {
        $service = $this->serviceFactory->create();
        $this->delete("/api/super-admin/service/" . $service->id);
        $this->assertDatabaseMissing("services", ["id" => $service->id]);
    }

    public function testASuperAdminCanGetServiceListFilteredBySearchExpression()
    {
        $limit = 5;
        $currentPage = 1;
        $totalPages = 2;
        $totalRecords = 8;

        $this->serviceFactory->count(5)->create([
            "name" => "len",
        ]);

        $this->serviceFactory->count(8)->create([
            "name" => "John",
        ]);

        $attributes = [
            "searchExpression" => "John",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getServicesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetAllServiceList()
    {
        $this->serviceFactory->count(5)->create([
            "name" => "len",
        ]);

        $this->serviceFactory->count(8)->create([
            "name" => "John",
        ]);

        $limit = 5;
        $currentPage = 1;
        $totalPages = 3;
        $totalRecords = 13;

        $attributes = [
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getServicesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }

    public function testASuperAdminCanGetServiceListFilteredByVisitPurpose()
    {
        $this->serviceFactory
            ->count(5)
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])
            ->create();
        $this->serviceFactory
            ->count(8)
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 2])
            ->create();

        $limit = 5;
        $currentPage = 2;
        $totalPages = 2;
        $totalRecords = 8;

        $attributes = [
            "visitPurposeId" => "2",
            "page" => $currentPage,
            "limit" => $limit,
        ];

        $response = $this->get(
            route("super-admin.getServicesList", $attributes),
        )->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals(
            $responseArray->pagination->currentPage,
            $currentPage,
        );
        $this->assertEquals(
            $responseArray->pagination->totalPages,
            $totalPages,
        );
        $this->assertEquals(
            $responseArray->pagination->totalRecords,
            $totalRecords,
        );
        $this->assertEquals($responseArray->pagination->pageLimit, $limit);
    }
}
