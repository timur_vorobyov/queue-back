<?php

namespace Tests\Api\Admin\SuperAdmin\VisitPurpose;

class CRUDVisitPurposeRequestStub
{
    private array $storeActionData;
    public function __construct(array $storeActionData)
    {
        $this->storeActionData = $storeActionData;
    }

    public function toDatabaseFormat(): array
    {
        return [
            "name" => $this->storeActionData["name"],
            "series" => $this->storeActionData["series"],
        ];
    }
}
