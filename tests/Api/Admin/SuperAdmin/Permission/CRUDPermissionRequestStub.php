<?php

namespace Tests\Api\Admin\SuperAdmin\Permission;

class CRUDPermissionRequestStub
{
    private array $storeActionData;
    public function __construct(array $storeActionData)
    {
        $this->storeActionData = $storeActionData;
    }

    public function toDatabaseFormat(): array
    {
        return [
            "name" => $this->storeActionData["name"],
            "guard_name" => $this->storeActionData["guardName"],
        ];
    }
}
