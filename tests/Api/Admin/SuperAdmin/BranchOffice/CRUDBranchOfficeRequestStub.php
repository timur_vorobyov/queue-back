<?php

namespace Tests\Api\Admin\SuperAdmin\BranchOffice;

class CRUDBranchOfficeRequestStub
{
    private array $storeActionData;
    public function __construct(array $storeActionData)
    {
        $this->storeActionData = $storeActionData;
    }

    public function toDatabaseFormat(): array
    {
        return [
            "initial" => $this->storeActionData["initial"],
            "town_id" => $this->storeActionData["townId"],
            "name" => $this->storeActionData["name"],
            "timer" => $this->storeActionData["timer"],
        ];
    }
}
