<?php

namespace Tests\Api\Admin\SuperAdmin\Role;

class CRUDRoleRequestStub
{
    private array $storeActionData;
    public function __construct(array $storeActionData)
    {
        $this->storeActionData = $storeActionData;
    }

    public function toDatabaseFormat(): array
    {
        return [
            "name" => $this->storeActionData["name"],
        ];
    }
}
