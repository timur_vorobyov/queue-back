<?php

namespace Tests\Api\Admin\SuperAdmin\Role;

use App\Modules\Permission\Permission;
use App\Modules\Role\Role;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CRUDRoleUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $roleFactory, $permissionFactory;
    private Role $role;

    protected function setUp(): void
    {
        parent::setUp();
        $this->role = app(Role::class);
        $this->permissionFactory = Permission::factory();
        $this->roleFactory = Role::factory();
    }

    public function testASuperAdminCanCreateRole()
    {
        $this->withoutExceptionHandling();
        $permissionIds = $this->permissionFactory
            ->count(2)
            ->create()
            ->map(function ($permission) {
                return $permission->id;
            })
            ->toArray();

        $attributes = [
            "name" => $this->faker->word,
            "permissionIds" => $permissionIds,
        ];

        $requestStub = new CRUDRoleRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();

        $this->post(route("super-admin.storeRole", $attributes))->assertStatus(
            200,
        );
        foreach ($permissionIds as $permissionId) {
            $this->assertDatabaseHas("role_has_permissions", [
                "permission_id" => $permissionId,
            ]);
        }

        $this->assertDatabaseHas("roles", $modifiedAttributes);
    }

    public function testASuperAdminCanUpdateRole()
    {
        $this->withoutExceptionHandling();
        $role = $this->roleFactory->create();

        $attributes = [
            "id" => $role->id,
            "name" => $this->faker->word,
        ];

        $response = $this->put(
            "/api/super-admin/roles/" . $role->id,
            $attributes,
        )->assertStatus(200);

        $this->assertEquals("Role is updated", $response["message"]);
    }

    public function testASuperAdminCanDeleteRole()
    {
        $role = $this->roleFactory->create();
        $this->delete("/api/super-admin/role/" . $role->id);
        $this->assertDatabaseMissing("roles", ["id" => $role->id]);
    }
}
