<?php

namespace Tests\Api\Person\BranchOffice\UseCases;

use App\Modules\BranchOffice\BranchOffice;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowBranchOfficesUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $branchOfficeFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->branchOfficeFactory = BranchOffice::factory();
    }

    public function testBranchOfficesAreListedCorrectly()
    {
        $this->withoutExceptionHandling();
        $branchOffices = $this->branchOfficeFactory
            ->count(3)
            ->create(["town_id" => 1])
            ->map(function ($branchOffice) {
                return $branchOffice->toDomainEntity()->jsonSerialize();
            });

        $this->get(route("showBranchOffices", ["townId" => 1]))
            ->assertStatus(200)
            ->assertJson($branchOffices->toArray())
            ->assertJsonStructure([
                "*" => [
                    "id",
                    "branchOfficeName",
                    "townId",
                    "initial",
                    "townName",
                ],
            ]);
    }
}
