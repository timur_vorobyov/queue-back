<?php

namespace Tests\Api\Person\VisitPurpose\UseCases;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowVisitPurposeUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $visitPurposeFactory, $branchOfficeFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->branchOfficeFactory = BranchOffice::factory();
    }

    public function testVisitPurposesForUserSettingsAreListedCorrectly()
    {
        $this->withoutExceptionHandling();

        $visitPurposes = $this->visitPurposeFactory
            ->count(3)
            ->hasAttached($this->branchOfficeFactory, ["branch_office_id" => 1])
            ->create()
            ->map(function ($visitPurpose) {
                return $visitPurpose->toDomainEntity()->jsonSerialize();
            });

        $this->get(route("showVisitPurposes", ["branchOfficeId" => 1]))
            ->assertStatus(200)
            ->assertJson($visitPurposes->toArray())
            ->assertJsonStructure([
                "*" => ["id", "name", "series"],
            ]);
    }
}
