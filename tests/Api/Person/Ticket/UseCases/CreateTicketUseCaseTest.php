<?php

namespace Tests\Api\Person\Ticket\UseCases;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Person\Jobs\SmsSenderJob;
use App\Modules\Ticket\Ticket;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Testing\Fakes\BusFake;
use Illuminate\Support\Testing\Fakes\EventFake;
use Tests\Api\Person\Ticket\RequestStubs\StoreTicketRequestStub;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;

class CreateTicketUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private EventFake $eventFake;
    private BusFake $busFake;
    private FactoryObject $statusFactory;
    private Ticket $ticket;

    protected function setUp(): void
    {
        parent::setUp();
        $this->eventFake = Event::fake();
        $this->busFake = Bus::fake();
        $this->statusFactory = Status::factory();
        $this->ticket = app(Ticket::class);
    }

    public function testAUserCanCreateATicket()
    {
        $this->withoutExceptionHandling();
        $this->statusFactory->create(["id" => 1, "name" => "new"]);

        $attributes = [
            "visitPurposeId" => VisitPurpose::factory()->create()->id,
            "customerPhoneNumber" => "900201200",
            "branchOfficeId" => BranchOffice::factory()->create()->id,
        ];

        $this->post("/api/tickets", $attributes)->assertStatus(200);

        $requestStub = new StoreTicketRequestStub($attributes);
        $modifiedAttributes = $requestStub->toDatabaseFormat();

        $savedTicket = $this->ticket::with('branchOffice', 'service', 'visitPurpose')
            ->where('status_id', 1)->first()->toDomainEntity();

        $reportTicketsData = [
            "ticket_id" => $savedTicket->getId(),
            "ticket_full_number" => $savedTicket->getFullTicketNumberWithoutDash(),
            "visit_purpose_name" => $savedTicket->getEVisitPurpose()->getName(),
            "customer_phone_number" => $savedTicket->getCustomerPhoneNumber(),
            "service_name" => "",
            "crm_customer_id" => null,
            "user_first_name" => null,
            "date_created_at" => date("Y-m-d", strtotime($savedTicket->getCreatedAt())),
            "time_created_at" => date("H:i:s", strtotime($savedTicket->getCreatedAt())),
            "branch_office_name" => $savedTicket->getBranchOfficeFullName(),
        ];

        $this->busFake->assertDispatched(SmsSenderJob::class);
        $this->eventFake->assertDispatched(TicketPendingEvent::class);
        $this->assertDatabaseHas("tickets", $modifiedAttributes);
        $this->assertDatabaseHas("report_tickets", $reportTicketsData);
    }
}
