<?php

namespace Tests\Api\Person\Ticket\UseCases;

use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\Events\TicketServiceEvent;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Testing\Fakes\EventFake;
use Tests\TestCase;

class ServeTicketUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private EventFake $eventFake;
    private FactoryObject $userFactory,
        $ticketFactory,
        $visitPurposeFactory,
        $statusFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->eventFake = Event::fake();
        $this->userFactory = User::factory();
        $this->ticketFactory = Ticket::factory();
        $this->statusFactory = Status::factory();
    }

    public function testAUserCanNotServeTicketAsVisitPurposesWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->create();

        $ticket = $this->ticketFactory->create([
            "status_id" => 3,
            "user_id" => $user->id,
        ]);

        $attributes = [
            "ticketId" => $ticket->id,
            "userId" => $user->id,
        ];

        $response = $this->put(route("serveTicket", $attributes))->assertStatus(
            200,
        );

        $this->assertEquals("The visit goal wasn't chosen", $response["data"]);
    }

    public function testAUserCanNotServeTicketAsDeskNumberWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create(["desk_number" => null]);

        $ticket = $this->ticketFactory->create([
            "status_id" => 3,
            "user_id" => $user->id,
        ]);

        $attributes = [
            "ticketId" => $ticket->id,
            "userId" => $user->id,
        ];

        $response = $this->put(route("serveTicket", $attributes))->assertStatus(
            200,
        );

        $this->assertEquals(
            "The number of desk wasn't chosen",
            $response["data"],
        );
    }

    public function testAUserCanServeTicket()
    {
        $this->withoutExceptionHandling();

        $this->statusFactory->create(["id" => 2, "name" => "invited"]);
        $this->statusFactory->create(["id" => 3, "name" => "serving"]);

        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create();

        $ticket = $this->ticketFactory->create([
            "status_id" => 2,
            "user_id" => $user->id,
            "visit_purpose_id" => 1,
            "branch_office_id" => $user->branch_office_id,
        ]);
        $attributes = [
            "ticketId" => $ticket->id,
            "userId" => $user->id,
        ];

        $response = $this->put(route("serveTicket", $attributes))->assertStatus(
            200,
        );

        $this->eventFake->assertDispatched(TicketAcceptEvent::class);
        $this->eventFake->assertDispatched(TicketServiceEvent::class);
        $this->assertEquals("Client is serving", $response["data"]);
    }
}
