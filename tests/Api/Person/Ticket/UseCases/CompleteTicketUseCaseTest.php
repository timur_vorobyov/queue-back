<?php

namespace Tests\Api\Person\Ticket\UseCases;

use App\Modules\Service\Service;
use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Events\TicketServiceEvent;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Testing\Fakes\EventFake;
use Tests\TestCase;

class CompleteTicketUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private EventFake $eventFake;
    private FactoryObject $userFactory,
        $serviceFactory,
        $ticketFactory,
        $statusFactory,
        $visitPurposeFactory;
    private Ticket $ticket;
    private int $visitPurposeId;

    protected function setUp(): void
    {
        parent::setUp();
        $this->eventFake = Event::fake();
        $this->userFactory = User::factory();
        $this->serviceFactory = Service::factory();
        $this->ticketFactory = Ticket::factory();
        $this->ticket = app(Ticket::class);
        $this->visitPurposeFactory = VisitPurpose::factory();
        $this->statusFactory = Status::factory();
        $this->visitPurposeId = $this->visitPurposeFactory->create()->id;
    }

    public function testAUserCanNotCompleteTicketAsVisitPurposesWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->create();
        $service = $this->serviceFactory->create();
        $ticket = $this->ticketFactory->create([
            "status_id" => 4,
            "user_id" => $user->id,
        ]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id,
            "serviceId" => $service->id,
            "visitPurposeId" => $this->visitPurposeId,
        ];

        $response = $this->put(
            route("completeTicket", $attributes),
        )->assertStatus(200);

        $this->assertEquals("The visit goal wasn't chosen", $response["data"]);
    }

    public function testAUserCanNotCompleteTicketAsDeskNumberWasNotChosen()
    {
        $this->withoutExceptionHandling();

        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create(["desk_number" => null]);
        $service = $this->serviceFactory->create();
        $ticket = $this->ticketFactory->create([
            "status_id" => 4,
            "user_id" => $user->id,
        ]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id,
            "serviceId" => $service->id,
            "visitPurposeId" => $this->visitPurposeId,
        ];

        $response = $this->put(
            route("completeTicket", $attributes),
        )->assertStatus(200);

        $this->assertEquals(
            "The number of desk wasn't chosen",
            $response["data"],
        );
    }

    public function testAUserCanCompleteTicket()
    {
        $this->withoutExceptionHandling();
        $this->statusFactory->create(["id" => 3, "name" => "serving"]);
        $this->statusFactory->create(["id" => 4, "name" => "completed"]);
        $service = $this->serviceFactory->create();
        $user = $this->userFactory->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])->create();

        $ticket = $this->ticketFactory->create([
            "status_id" => 3,
            "user_id" => $user->id,
            "visit_purpose_id" => 1,
            "branch_office_id" => $user->branch_office_id,
        ]);

        $attributes = [
            "userId" => $user->id,
            "ticketId" => $ticket->id,
            "serviceId" => $service->id,
            "visitPurposeId" => $this->visitPurposeId,
        ];

        $response = $this->put(
            route("completeTicket", $attributes),
        )->assertStatus(200);

        $updatedTicket = $this->ticket::find($ticket->id);

        $this->assertEquals($service->id, $updatedTicket->service_id);
        $this->eventFake->assertDispatched(TicketServiceEvent::class);
        $this->assertEquals("Service is completed", $response["data"]);
    }
}
