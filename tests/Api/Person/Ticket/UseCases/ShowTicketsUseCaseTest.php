<?php

namespace Tests\Api\Person\Ticket\UseCases;

use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTicketsUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $ticketFactory, $userFactory, $statusFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->ticketFactory = Ticket::factory();
        $this->userFactory = User::factory();
        $this->statusFactory = Status::factory();
    }

    public function testFullTicketsAreListedCorrectly()
    {
        $this->withoutExceptionHandling();
        $this->statusFactory->create(["id" => 1, "name" => "new"]);

        $firstTicket = $this->ticketFactory
            ->create([
                "branch_office_id" => 1,
                "status_id" => 1,
                "user_id" => 1,
                "visit_purpose_id" => 1,
            ])::with('visitPurpose', 'branchOffice')
            ->where('id', 1)
            ->first()->toDomainEntity()->jsonSerialize();

        $secondTicket = $this->ticketFactory
            ->create([
                "id" => 2,
                "branch_office_id" => 1,
                "status_id" => 1,
                "user_id" => 1,
                "visit_purpose_id" => 2,
            ])::with('visitPurpose', 'branchOffice')
            ->where('id', 2)
            ->first()->toDomainEntity()->jsonSerialize();

        $attributes = [
            "branchOfficeId" => 1,
            "statusName" => "new",
            "visitPurposesIds" => [1,2],
        ];

        $this->get(route("getFullTicketsList", $attributes))
            ->assertStatus(200)
            ->assertJson([$firstTicket,$secondTicket])
            ->assertJsonStructure([
                "*" => [
                    "id",
                    "crmCustomerId",
                    "customerPhoneNumber",
                    "createdAt",
                    "pendingTime",
                    "servedAt",
                    "canceledAt",
                    "completedAt",
                    "invitedAt",
                    "visitPurposeName",
                    "visitPurposeId",
                    "ticketFullNumber",
                ],
            ]);
    }

    public function testGetTicket()
    {
        $this->withoutExceptionHandling();
        $this->statusFactory->create(["id" => 1, "name" => "new"]);

        $ticket = $this->ticketFactory
            ->create([
                "branch_office_id" => 1,
                "status_id" => 1,
                "user_id" => 1,
                "visit_purpose_id" => 1,
            ])->toDomainEntity()->jsonSerialize();

        $attributes = [
            "branchOfficeId" => 1,
            "statusName" => "new",
            "userId" => 1,
            "visitPurposesIds" => [1],
        ];

        $response =$this->get(route("getTicket", $attributes))
            ->assertStatus(200);

        $responseArray = json_decode($response->content());
        $this->assertEquals($responseArray->id, $ticket['id']);

    }
}
