<?php

namespace Tests\Api\Person\Ticket\RequestStubs;

class StoreTicketRequestStub
{
    private $visitPurposeId;
    private $customerPhoneNumber;
    private $branchOfficeId;

    public function __construct(array $attributes)
    {
        $this->visitPurposeId = $attributes["visitPurposeId"];
        $this->customerPhoneNumber = $attributes["customerPhoneNumber"];
        $this->branchOfficeId = $attributes["branchOfficeId"];
    }

    public function toDatabaseFormat(): array
    {
        return [
            "visit_purpose_id" => $this->visitPurposeId,
            "branch_office_id" => $this->branchOfficeId,
            "customer_phone_number" => $this->customerPhoneNumber,
            "status_id" => 1,
            "user_id" => null,
            "service_id" => null,
        ];
    }
}
