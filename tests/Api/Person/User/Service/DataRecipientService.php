<?php

namespace Tests\Api\Person\User\Service;

use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DataRecipientService extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $userFactory, $ticketFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userFactory = User::factory();
        $this->ticketFactory = Ticket::factory();
    }

    public function testGetUsersAverageServiceTime()
    {
        $this->ticketFactory->count(3)->create([
            "created_at" => "2021-01-31 11:56:38",
            "completed_at" => "2021-01-31 12:03:56",
        ]);

        $userServiceTime =
            strtotime("2021-01-31 12:03:56") - strtotime("2021-01-31 11:56:38");
        $usersAverageServiceTime = date("h:i:s", ($userServiceTime * 3) / 3);

        $response = $this->get(route("getUsersAverageServiceTime"));
        $this->assertEquals(
            $usersAverageServiceTime,
            $response["usersAverageServiceTime"],
        );
    }

    public function testGetUserAverageServiceTime()
    {
        $user = $this->userFactory->create();
        $this->ticketFactory->count(10)->create([
            "visit_purpose_id" => $user->visit_purpose_id,
            "user_id" => $user->id,
            "invited_at" => "2021-01-31 11:56:38",
            "completed_at" => "2021-01-31 12:03:56",
        ]);

        $userServiceTime =
            strtotime("2021-01-31 12:03:56") - strtotime("2021-01-31 11:56:38");
        $userAverageServiceTime = date("h", ($userServiceTime * 10) / 10);

        $response = $this->get(
            route("getUserAverageServiceTime", [
                "userId" => $user->id,
                "visitPurposeId" => $user->visit_purpose_id,
            ]),
        );
        $this->assertEquals(
            $userAverageServiceTime,
            $response["userAverageServiceTime"],
        );
    }

    public function testGetCustomersAmountUserServed()
    {
        $firstUser = $this->userFactory->create([
            "id" => 5,
            "visit_purpose_id" => 7,
        ]);
        $secondUser = $this->userFactory->create([
            "id" => 7,
            "visit_purpose_id" => 5,
        ]);

        $this->ticketFactory->count(3)->create([
            "visit_purpose_id" => $firstUser->visit_purpose_id,
            "user_id" => $firstUser->id,
            "created_at" => Carbon::now()->toDateString(),
            "completed_at" => Carbon::now()->toDateString(),
        ]);
        $this->ticketFactory->create([
            "visit_purpose_id" => $secondUser->visit_purpose_id,
            "user_id" => $secondUser->id,
            "created_at" => "2021-01-31 11:56:38",
            "completed_at" => "2021-01-31 12:03:56",
        ]);

        $response = $this->get(
            route("getCustomersAmountUserServed", [
                "userId" => $firstUser->id,
                "visitPurposeId" => $firstUser->visit_purpose_id,
            ]),
        );
        $this->assertEquals("3", $response["customersAmountUserServed"]);
    }
}
