<?php

namespace Tests\Api\Person\User\UseCases;

use App\Modules\Role\Role;
use App\Modules\User\User;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $userFactory, $roleFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userFactory = User::factory();
        $this->roleFactory = Role::factory();
    }

    public function testAUserCanLogin()
    {
        $user = $this->userFactory->create();
        $role = $this->roleFactory->create();
        $user->assignRole($role->name);
        $attributes = [
            "email" => $user->email,
            "password" => "123456789",
        ];

        $response = $this->post(route("login", $attributes))
            ->assertStatus(200)
            ->assertJsonStructure(["message", "accessToken", "id"]);

        $this->assertEquals("You are logged in", $response["message"]);
    }

    public function testAUserCanNotLogin()
    {
        $user = $this->userFactory->create();

        $attributes = [
            "email" => $user->email,
            "password" => "1234589",
        ];

        $response = $this->post(route("login", $attributes))
            ->assertStatus(200)
            ->assertJsonStructure(["message", "accessToken", "id"]);

        $this->assertEquals(
            "We can`t find an account with this credentials",
            $response["message"],
        );
        $this->assertEquals(false, $response["accessToken"]);
        $this->assertEquals(null, $response["id"]);
    }
}
