<?php

namespace Tests\Api\Person\Service\UseCases;

use App\Modules\Service\Service;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory as FactoryObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowServicesUseCaseTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private FactoryObject $serviceFactory, $visitPurposeFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serviceFactory = Service::factory();
        $this->visitPurposeFactory = VisitPurpose::factory();
    }

    public function testServicesAreListedCorrectly()
    {
        $this->withoutExceptionHandling();

        $service = $this->serviceFactory
            ->hasAttached($this->visitPurposeFactory, ["visit_purpose_id" => 1])
            ->create();


        $this->post(route("showServices", ["visitPurposeId" => 1]))
            ->assertStatus(200)
            ->assertJson([$service->toDomainEntity()->jsonSerialize()])
            ->assertJsonStructure([
                "*" => ["id", "name", "isActive"],
            ]);
    }
}
