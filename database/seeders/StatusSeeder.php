<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'name' => 'new',
        ]);
        DB::table('statuses')->insert([
            'name' => 'invited',
        ]);
        DB::table('statuses')->insert([
            'name' => 'serving',
        ]);
        DB::table('statuses')->insert([
            'name' => 'completed',
        ]);
        DB::table('statuses')->insert([
            'name' => 'declined',
        ]);
    }
}
