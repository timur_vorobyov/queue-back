<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserStatusSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Db::table('user_statuses')->insert(
            [
                ['name' => 'lunch time'],
                ['name' => 'break time'],
                ['name' => 'work time'],
                ['name' => 'offline time'],
            ]
        );
    }
}
