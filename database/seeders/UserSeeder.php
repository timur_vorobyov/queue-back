<?php

namespace Database\Seeders;

use App\Modules\User\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'branch_office_id' => null,
            'first_name' => 'User',
            'last_name' => 'User',
            'email' => 'user@gmail.com',
            'password' =>   Hash::make('123456789'),
            'desk_number' => null,
            'is_active' => true,
            'is_allowed_have_same_desk_number' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $user->assignRole('super admin');
    }
}
