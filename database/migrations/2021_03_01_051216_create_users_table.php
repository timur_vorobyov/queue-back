<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('branch_office_id')->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('password');
            $table->string('email')->unique();
            $table->string('desk_number')->nullable();
            $table->boolean('is_active');
            $table->timestamps();

            $table->foreign('branch_office_id')->references('id')->on('branch_offices')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
