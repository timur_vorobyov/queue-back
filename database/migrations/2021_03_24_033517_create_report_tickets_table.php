<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_tickets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ticket_id');
            $table->string('ticket_full_number');
            $table->string('visit_purpose_name');
            $table->string('service_name')->nullable();
            $table->string('customer_phone_number')->nullable();
            $table->unsignedBigInteger('crm_customer_id')->nullable();
            $table->date('date_created_at');
            $table->time('time_created_at');
            $table->string('customer_pending_time')->nullable();
            $table->string('customer_service_time')->nullable();
            $table->string('user_first_name')->nullable();
            $table->string('branch_office_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_tickets');
    }
}
