<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsTjMessageAndRuMessageColumnsToVisitPurposesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visit_purposes', function (Blueprint $table) {
            $table->string('tj_message')->default('');
            $table->string('ru_message')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visit_purposes', function (Blueprint $table) {
            $table->dropColumn('tj_message');
            $table->dropColumn('ru_message');
        });
    }
}
