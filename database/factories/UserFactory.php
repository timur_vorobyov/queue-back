<?php

namespace Database\Factories;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\User\User;
use App\Modules\UserStatus\UserStatus;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'branch_office_id' => BranchOffice::factory()->create()->id,
            'is_allowed_have_same_desk_number' => true,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->unique()->safeEmail,
            'password' =>  Hash::make('123456789'),
            'desk_number' => $this->faker->randomDigit.$this->faker->randomLetter,
            'is_active' => true,
        ];
    }
}
