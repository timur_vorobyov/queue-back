<?php


namespace Database\Factories;


use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Town\Town;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchOfficeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BranchOffice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->word(),
            'initial' => $this->faker->unique()->randomLetter,
            'town_id' => Town::factory()->create()->id,
            'timer' => $this->faker->numberBetween($min = 1000, $max = 5000),
        ];
    }

}
