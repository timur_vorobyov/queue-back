<?php

namespace Database\Factories;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ticket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'visit_purpose_id' => VisitPurpose::factory()->create()->id,
            'user_id' => 1,
            'branch_office_id' => BranchOffice::factory()->create()->id,
            'status_id' => $this->faker->numberBetween($min = 1, $max = 5),
            'service_id' => 1,
            'ticket_number' => $this->faker->unique()->numberBetween($min = 1, $max = 10000),
            'customer_phone_number' => '900201200',
            'invited_at' => null,
            'served_at' => null,
            'completed_at' => null,
            'canceled_at' => null,
            'is_salom_cache' => false,
        ];
    }
}
