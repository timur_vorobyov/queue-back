<?php

namespace App\Requests\Person;

use Illuminate\Validation\Factory;

abstract class ARequest
{
    abstract public function toArray(): array;
    abstract public function validationRules(): array;

    public function validate(): void
    {
        $validationFactory = app(Factory::class);
        $validationFactory
            ->make($this->toArray(), $this->validationRules())
            ->validate();
    }
}
