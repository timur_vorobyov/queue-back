<?php

namespace App\Interfaces;

interface IJsonSerializable
{
    public function superAdminJsonSerialize(): array;
    public function adminJsonSerialize(): array;
    public function jsonSerialize(): array;
}
