<?php

namespace App\Interfaces;

use App\Requests\Person\ARequest;

interface IUseCase
{
    public function perform(ARequest $request);
}
