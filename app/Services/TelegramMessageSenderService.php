<?php

namespace App\Services;

use App\Services\Interfaces\ITelegramMessageSenderService;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramMessageSenderService implements ITelegramMessageSenderService
{
    private string $channelId = "-1001217788471";

    public function sendMessage(string $message): void
    {
        Telegram::sendMessage([
            "chat_id" => $this->channelId,
            "text" => $message,
        ]);
    }
}
