<?php


namespace App\Services\Interfaces;


interface ISmsService
{
    public function send(string $customerPhoneNumber, string $message): void;
}
