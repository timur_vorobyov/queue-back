<?php


namespace App\Services\Interfaces;


interface ICallSenderService
{
    public function send(int $customerPhoneNumber): void;
}
