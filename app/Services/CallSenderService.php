<?php

namespace App\Services;

use App\Services\Interfaces\ICallSenderService;
use App\Services\Interfaces\ITelegramMessageSenderService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

class CallSenderService implements ICallSenderService
{
    private ITelegramMessageSenderService $telegramMessageSender;

    public function __construct(ITelegramMessageSenderService $telegramMessageSender)
    {
        $this->telegramMessageSender = $telegramMessageSender;
    }

    public function send(int $customerPhoneNumber): void
    {
        $token ='tJpftnqoExF9svuGwLsijLnYqlns6qc3Q7SGprc9CplgP0lyuf69fStFsb2J';

        try {
            $client = app(Client::class);
            $ivrUrl = "http://192.168.15.155:9990/call";

            $response = $client->request('POST', $ivrUrl, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' .$token
                ],
                'body' => json_encode([
                    'audio_id' => 30,
                    'phone_number' => "$customerPhoneNumber"
                ])
            ]);

            if ($response->getStatusCode() != 200 && $response->getStatusCode() != 202) {
                $this->sendExceptionToTelegram($response);
            }

            if ($response->getStatusCode() == 200) {
                Log::channel("call")->info(
                    "The call was sent to " . $customerPhoneNumber,
                );
            }
        } catch (\Exception $e) {
            $this->telegramMessageSender->sendMessage($e->getMessage());
        }
    }

    private function sendExceptionToTelegram(ResponseInterface $response): void
    {
        $message =
            config("app.name") .
            ": Call was not sent: { code:" .
            $response->getStatusCode() .
            ", message:" .
            $response->getReasonPhrase() .
            "}";
        $this->telegramMessageSender->sendMessage($message);
    }
}
