<?php

namespace App\Modules\VisitPurpose\Person\Requests;

class ShowVisitPurposeRequest extends AVisitPurposeRequest
{
    public function __construct(int $branchOfficeId)
    {
        $this->branchOfficeId = $branchOfficeId;
    }

    public function toArray(): array
    {
        return [
            'branchOfficeId' => $this->branchOfficeId
        ];
    }

    public function validationRules(): array
    {
        return [
            'branchOfficeId' => "required"
        ];
    }
}
