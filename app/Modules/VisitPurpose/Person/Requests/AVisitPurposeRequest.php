<?php

namespace App\Modules\VisitPurpose\Person\Requests;

use App\Requests\Person\ARequest;

abstract class AVisitPurposeRequest extends ARequest
{
    protected ?int $branchOfficeId;
    protected ?int $userId;

    public function getBranchOfficeId(): ?int
    {
        return $this->branchOfficeId;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }
}
