<?php

namespace App\Modules\VisitPurpose\Person;

use App\Modules\User\User;
use App\Modules\VisitPurpose\Person\Interfaces\IVisitPurposeRepository;
use App\Modules\VisitPurpose\VisitPurpose;

class VisitPurposeRepository implements IVisitPurposeRepository
{
    private VisitPurpose $visitPurpose;
    private User $user;

    public function __construct()
    {
        $this->visitPurpose = app(VisitPurpose::class);
        $this->user = app(User::class);
    }

    public function getVisitPurposesList(int $branchOfficeId): object
    {
        $visitPurposes = $this->visitPurpose
            ::whereHas("branchOffices", function ($q) use ($branchOfficeId) {
                $q->where('branch_office_id', $branchOfficeId);
            })->get();
        return $visitPurposes->map(function ($visitPurpose) {
            return $visitPurpose->toDomainEntity()->jsonSerialize();
        });
    }

}
