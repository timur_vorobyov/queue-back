<?php

namespace App\Modules\VisitPurpose\Person\Interfaces;

interface IVisitPurposeRepository
{
    public function getVisitPurposesList(int $branchOfficeId): object;
}
