<?php

namespace App\Modules\VisitPurpose\Person\UseCases;

use App\Requests\Person\ARequest;

class ShowVisitPurposeUseCase extends AVisitPurposeUseCase
{
    public function perform(ARequest $request)
    {
        $request->validate();

        return $this->visitPurposeRepository->getVisitPurposesList(
            $request->getBranchOfficeId(),
        );
    }
}
