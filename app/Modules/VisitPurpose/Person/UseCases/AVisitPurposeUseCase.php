<?php

namespace App\Modules\VisitPurpose\Person\UseCases;

use App\Interfaces\IUseCase;
use App\Modules\VisitPurpose\Person\Interfaces\IVisitPurposeRepository;
use App\Requests\Person\ARequest;

abstract class AVisitPurposeUseCase implements IUseCase
{
    protected IVisitPurposeRepository $visitPurposeRepository;

    public function __construct(IVisitPurposeRepository $visitPurposeRepository)
    {
        $this->visitPurposeRepository = $visitPurposeRepository;
    }
}
