<?php

namespace App\Modules\VisitPurpose;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Service\Service;
use App\Modules\User\User;
use Database\Factories\VisitPurposeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisitPurpose extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function newFactory()
    {
        return VisitPurposeFactory::new();
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function branchOffices()
    {
        return $this->belongsToMany(BranchOffice::class);
    }

    public function toDomainEntity(): EVisitPurpose
    {
        $relations = $this->getRelations();

        return new EVisitPurpose(
            $this->id,
            $this->name,
            $this->series,
            key_exists('services', $relations) && !is_null($this->services) ? $this->services : null,
            $this->tj_message,
            $this->ru_message
        );
    }
}
