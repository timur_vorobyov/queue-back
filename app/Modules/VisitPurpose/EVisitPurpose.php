<?php

namespace App\Modules\VisitPurpose;

use App\Interfaces\IJsonSerializable;

class EVisitPurpose implements IJsonSerializable
{
    private int $id;
    private string $name;
    private string $series;
    private ?object $services;
    private string $tjMessage;
    private string $ruMessage;

    public function __construct(
        int $id,
        string $name,
        string $series,
        ?object $services,
        string $tjMessage,
        string $ruMessage
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->series = $series;
        $this->services = $services;
        $this->tjMessage = $tjMessage;
        $this->ruMessage = $ruMessage;
    }

    public function getTjMessage(): string
    {
        return  $this->tjMessage;
    }

    public function getRuMessage(): string
    {
        return $this->ruMessage;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "series" => $this->series,
        ];
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "series" => $this->series,
            "services" => $this->services ? $this->services->map(function ($service) {
                return $service->toDomainEntity()->superAdminJsonSerialize();
            }) : null,
            "tjMessage" => $this->tjMessage,
            "ruMessage" => $this->ruMessage,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSeries(): string
    {
        return $this->series;
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }
}
