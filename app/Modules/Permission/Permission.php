<?php

namespace App\Modules\Permission;

use Database\Factories\PermissionFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    use HasFactory;

    protected $fillable = ["name", "guard_name"];

    protected static function newFactory()
    {
        return PermissionFactory::new();
    }

    public function toDomainEntity(): EPermission
    {
        return new EPermission($this->id, $this->name);
    }
}
