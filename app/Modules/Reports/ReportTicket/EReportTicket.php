<?php


namespace App\Modules\Reports\ReportTicket;


class EReportTicket
{
    private int $id;
    private int $ticketId;
    private string $ticketFullNumber;
    private string $visitPurposeName;
    private ?string $serviceName;
    private ?string $customerPhoneNumber;
    private ?int $crmCustomerId;
    private string $dateCreatedAt;
    private string $timeCreatedAt;
    private string $customerPendingTime;
    private string $customerServiceTime;
    private ?string $userFirstName;
    private string $branchOfficeName;


    public function __construct(
        int $id,
        int $ticketId,
        string $ticketFullNumber,
        string $visitPurposeName,
        ?string $serviceName,
        ?string $customerPhoneNumber,
        ?int $crmCustomerId,
        string $dateCreatedAt,
        string $timeCreatedAt,
        string $customerPendingTime,
        string $customerServiceTime,
        ?string $userFirstName,
        string $branchOfficeName
    ) {
        $this->id = $id;
        $this->ticketId = $ticketId;
        $this->ticketFullNumber = $ticketFullNumber;
        $this->visitPurposeName = $visitPurposeName;
        $this->serviceName = $serviceName;
        $this->customerPhoneNumber = $customerPhoneNumber;
        $this->crmCustomerId = $crmCustomerId;
        $this->dateCreatedAt = $dateCreatedAt;
        $this->timeCreatedAt = $timeCreatedAt;
        $this->customerPendingTime = $customerPendingTime;
        $this->customerServiceTime = $customerServiceTime;
        $this->userFirstName = $userFirstName;
        $this->branchOfficeName = $branchOfficeName;
    }

    public function reportJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "ticketFullNumber"=> $this->ticketFullNumber,
            "visitPurposeName" => $this->visitPurposeName,
            "serviceName" => $this->serviceName,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "crmCustomerId" => $this->crmCustomerId,
            "date" => $this->dateCreatedAt,
            "time" =>  $this->timeCreatedAt,
            "customerPendingTime" => $this->customerPendingTime,
            "customerServiceTime" => $this->customerServiceTime,
            "userFirstName"=> $this->userFirstName,
            "branchOfficeName" =>  $this->branchOfficeName
        ];
    }

    public function exportJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "ticketFullNumber"=> $this->ticketFullNumber,
            "visitPurposeName" => $this->visitPurposeName,
            "serviceName" => $this->serviceName,
            "customerPhoneNumber" => $this->customerPhoneNumber,
            "crmCustomerId" => $this->crmCustomerId,
            "date" => $this->dateCreatedAt,
            "time" =>  $this->timeCreatedAt,
            "customerPendingTime" => $this->customerPendingTime,
            "customerServiceTime" => $this->customerServiceTime,
            "userFirstName"=> $this->userFirstName,
            "branchOfficeName" =>  $this->branchOfficeName
        ];
    }
}
