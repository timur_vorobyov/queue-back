<?php


namespace App\Modules\Reports\ReportTicket;


use Illuminate\Database\Eloquent\Model;

class ReportTicket extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function toDomainEntity(): EReportTicket
    {
        return new EReportTicket(
            $this->id,
            $this->ticket_id,
            $this->ticket_full_number,
            $this->visit_purpose_name,
            $this->service_name,
            $this->customer_phone_number,
            $this->crm_customer_id,
            $this->date_created_at,
            $this->time_created_at,
            $this->customer_pending_time,
            $this->customer_service_time,
            $this->user_first_name,
            $this->branch_office_name,
        );
    }
}
