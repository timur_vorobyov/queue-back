<?php


namespace App\Modules\Reports\ReportUserActionTime;


use App\Modules\User\User;
use Illuminate\Database\Eloquent\Model;

class ReportUserActionTime extends Model
{
    protected $guarded = [];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function toDomainEntity(): EReportUserActionTime
    {
        $relations = $this->getRelations();

        return new EReportUserActionTime(
            $this->id,
            $this->user_id,
            $this->lunch_time,
            $this->break_time,
            $this->work_time,
            $this->offline_time,
            $this->created_at,
            key_exists('user', $relations) && !is_null($this->user) ? $this->user->toDomainEntity() : null,
        );
    }
}
