<?php

namespace App\Modules\User\Admin\SuperAdmin;

use App\Modules\Reports\ReportUserActionTime\ReportUserActionTime;
use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;


class UsersExport implements FromCollection, WithHeadings, WithEvents, ShouldAutoSize
{
    use Exportable;

    private Status $status;
    private ?int $branchOfficeId;
    private ?int $statusId;
    private ?int $userId;
    private ?string $searchExpression;
    private ?string $dateFrom;
    private ?string $dateTo;
    private ReportUserActionTime $reportUserActionTime;
    private User $user;

    public function __construct(array $filterData)
    {
        $this->dateFrom = $filterData["dateFrom"];
        $this->dateTo = $filterData["dateTo"];
        $this->branchOfficeId = $filterData["branchOfficeId"];
        $this->statusId = $filterData["statusId"];
        $this->userId = $filterData["userId"];
        $this->searchExpression = $filterData["searchExpression"];
        $this->reportUserActionTime = app(ReportUserActionTime::class);
        $this->user = app(User::class);
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $dateFrom = $this->dateFrom == "1970-01-01" ? null : date('Y-m-d' , strtotime($this->dateFrom)).' 00:00:00';
        $dateTo = $this->dateTo == "1970-01-01" ? null : date('Y-m-d' , strtotime($this->dateTo)).' 23:59:59';

        $userQuery = $this->user::with("branchOffice");

        if ($this->branchOfficeId) {
            $userQuery->where("branch_office_id", $this->branchOfficeId);
        }

        if ($this->statusId) {
            $userQuery->where("status_id", $this->statusId);
        }

        if ($this->searchExpression) {
            $userQuery
                ->where("first_name", "LIKE", "%$this->searchExpression%")
                ->orWhere("last_name", "LIKE", "%$this->searchExpression%")
                ->orWhere("email", "LIKE", "%$this->searchExpression%");
        }

        $userIds = $userQuery->get()->pluck('id')->all();

        $modifiedUsersId = '(' . implode(',', $userIds) .')';

        if ($modifiedUsersId != "()") {
            $userActionTimeQuery = $this->reportUserActionTime::with('user')
                ->whereRaw("user_id IN $modifiedUsersId");
        } else {
            $userActionTimeQuery = $this->reportUserActionTime::with('user');
        }

        if($dateFrom && $dateTo) {
            $userActionTimeQuery
                ->where("created_at", '>=', $dateFrom)
                ->where("created_at", '<=' , $dateTo);
        }

        if ($this->userId) {
            $userActionTimeQuery->where("user_id", $this->userId);
        }

        return $userActionTimeQuery
            ->get()
            ->map(function ($userActionTime) {
                return $userActionTime->toDomainEntity()->exportJsonSerialize();
            });
    }

    public function headings(): array
    {
        return [
            '#',
            "Полное имя юзера",
            "Время обеда",
            "Время работы",
            "Время перерыва",
            "Время отсутствия",
            "Время создания"
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        return [
            AfterSheet::class => function(AfterSheet $event) {
            $event->sheet->styleCells(
                'A1:L1',
                [
                'font' => [
                    'bold' => true
                ]
            ]);
            }
        ];
    }
}
