<?php

namespace App\Modules\User;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\UserStatus\UserStatus;
use App\Modules\VisitPurpose\VisitPurpose;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRoles;

    protected $guarded = [];
    protected $hidden = ["password", "remember_token"];

    protected static function newFactory()
    {
        return UserFactory::new();
    }

    public function branchOffice()
    {
        return $this->belongsTo(BranchOffice::class);
    }

    public function visitPurposes()
    {
        return $this->belongsToMany(VisitPurpose::class);
    }

    public function userStatus()
    {
        return $this->belongsTo(UserStatus::class, 'status_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function toDomainEntity(): EUser
    {
        $relations = $this->getRelations();

        return new EUser(
            $this->id,
            $this->branch_office_id,
            key_exists('visitPurposes', $relations) && !is_null($this->visitPurposes) ? $this->visitPurposes : null,
            $this->first_name,
            $this->last_name,
            $this->email,
            $this->desk_number,
            $this->is_active,
            key_exists('branchOffice', $relations) && !is_null($this->branchOffice) ? $this->branchOffice->toDomainEntity() : null,
            $this->created_at,
            $this->updated_at,
            key_exists('roles', $relations) ? $this->roles->first() : null,
            $this->is_allowed_have_same_desk_number,
            isset($this->userStatus) ? $this->userStatus->id : 4,
            isset($this->userStatus) ? $this->userStatus->name : 'offline',
        );
    }
}
