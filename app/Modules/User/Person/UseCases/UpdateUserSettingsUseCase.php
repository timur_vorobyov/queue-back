<?php


namespace App\Modules\User\Person\UseCases;


use App\Requests\Person\ARequest;

class UpdateUserSettingsUseCase extends AUserUseCase
{

    public function perform(ARequest $request): array
    {
        $request->validate();

        $userData = $this->userRepository->updateSettingsData($request->getUserSettingsData());

        if (gettype($userData) != 'string') {
            return $userData->jsonSerialize();
        }

        return ['errorMessage' => $userData];
    }
}
