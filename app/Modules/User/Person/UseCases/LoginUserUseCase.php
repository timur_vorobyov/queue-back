<?php

namespace App\Modules\User\Person\UseCases;

use App\Requests\Person\ARequest;

class LoginUserUseCase extends AUserUseCase
{
    public function perform(ARequest $request): array
    {
        $request->validate();

        $token = $this->userRepository->login(
            $request->getEmail(),
            $request->getPassword(),
        );

        if ($token) {
            $user = $this->userRepository->getUserByEmail($request->getEmail());
            return ["token" => $token, "userId" => $user->getId()];
        }

        return ["token" => null, "userId" => null];
    }
}
