<?php


namespace App\Modules\User\Person\UseCases;


use App\Requests\Person\ARequest;

class UpdateUserStatusUseCase extends AUserUseCase
{
    public function perform(ARequest $request): bool
    {
        $request->validate();

        $userDataBeforeBeingUpdated = $this->userRepository->getUserById($request->getUserId());
        $statusIsUpdated = $this->userRepository->updateStatus($request->getUserId(), $request->getUserStatusId());

        if ($statusIsUpdated) {
            $this->userRepository->updateUserActionTimeReport($userDataBeforeBeingUpdated);

            return true;
        }

        return false;
    }
}
