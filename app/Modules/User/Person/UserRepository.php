<?php


namespace App\Modules\User\Person;


use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Reports\ReportUserActionTime\ReportUserActionTime;
use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use App\Modules\User\EUser;
use App\Modules\User\Person\Interfaces\IUserRepository;
use App\Modules\User\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserRepository implements IUserRepository
{
    private User $user;
    private BranchOffice $branchOffice;
    private Ticket $ticket;
    private Status $status;
    private ReportUserActionTime $reportUserActionTime;

    public function __construct()
    {
        $this->user = app(User::class);
        $this->branchOffice = app(BranchOffice::class);
        $this->ticket = app(Ticket::class);
        $this->reportUserActionTime = app(ReportUserActionTime::class);
        $this->status = app(Status::class);
    }

    public function login(string $email, string $password): bool
    {
        $credentials = ['email' => $email, 'password' => $password];
        return auth()->attempt($credentials);
    }

    public function getUserByEmail(string $email): EUser
    {
        return $this->user::with('branchOffice', 'roles')->where('email', $email)->first()->toDomainEntity();
    }

    public function getUserById(int $userId): EUser
    {
        return $this->user::with('branchOffice', 'roles')->where('id', $userId)->first()->toDomainEntity();
    }

    public function updateSettingsData(array $newWorkData): string|EUser
    {
        $user = $this->user::find($newWorkData['userId']);
        $user->visitPurposes()->sync($newWorkData['visitPurposesIds']);
        $branchInitial = $user->branchOffice->initial;
        $fullDeskNumber = $newWorkData['deskNumber'] . $branchInitial;

         $userIsUpdated = $user->update([
            'branch_office_id' => $newWorkData['branchOfficeId'],
            'desk_number' => $fullDeskNumber,
        ]);

        if ($userIsUpdated) {
            if($newWorkData['userIdWithSameDeskNumber']) {
                $existedUser = $this->user::find($newWorkData['userIdWithSameDeskNumber']);
                $existedUser->update([
                    'desk_number' => null
                ]);
            }

            return $this->user::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')->find($user->id)->toDomainEntity();
        }

        return "User doesn't exist";
    }

    public function getAllTicketsFilteredByBranchOfficeId(int $branchOfficeId): object
    {
        $tickets = $this->ticket::select('completed_at','invited_at')->where('branch_office_id', $branchOfficeId)->get();
        return $tickets->map(function ($ticket) {
            return $ticket->toDomainEntity();
        });
    }

    public function getAllTicketsFilteredByUserIdVisitPurposesIds(int $userId, array $visitPurposesIds): object
    {
        $tickets = $this->ticket::select('completed_at','invited_at')
            ->where('user_id', $userId)
            ->whereIn('visit_purpose_id', $visitPurposesIds)
            ->get();

        return $tickets->map(function ($ticket) {
            return $ticket->toDomainEntity();
        });
    }

    public function getUsersMaxServiceTime(int $branchOfficeId): string|null
    {
        return DB::table('tickets')
            ->selectRaw('max(timediff(completed_at, invited_at)) as usersMaxServiceTime')
            ->where('branch_office_id', $branchOfficeId)
            ->first()->usersMaxServiceTime;
    }

    public function  getCustomersAmountUserServed(int $userId): string
    {
        return $this->ticket
            ->whereDate('completed_at', '=',date('Y-m-d'))
            ->where('user_id', $userId)
            ->get()
            ->count();
    }

    public function getDeskNumberExistingCount(string $deskNumber): int
    {
        return $this->user::where('desk_number', $deskNumber)->get()->count();
    }

    public function getUsersList(): object
    {
        return $this->user::with('roles')->get()->map(function ($user) {
            return $user->toDomainEntity()->jsonSerialize();
        });
    }

    public function getUsersFilteredByDeskNumber(string $deskNumber): object
    {
        return $this->user::with('visitPurposes', 'branchOffice')->where('desk_number', $deskNumber)->get()->map(function ($user) {
            return $user->toDomainEntity();
        });
    }

    public function getUserDataFilteredById (int $id): array
    {
        return $this->user::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')->find($id)->toDomainEntity()->jsonSerialize();
    }

    public function updateStatus(int $userId, int $statusId): bool
    {
        $user = $this->user->find($userId);
        return $user->update(['status_id' => $statusId, 'updated_at' => Carbon::now()]);
    }

    public function updateUserActionTimeReport(EUser $userDataBeforeBeingUpdated): bool
    {
        $dateFrom = Carbon::today()->toDateString().' 00:00:00';
        $dateTo = Carbon::today()->toDateString() .' 23:59:59';
        $userId = $userDataBeforeBeingUpdated->getId();
        $userDataAfterBeingUpdated = $this->user->find($userId)->toDomainEntity();
        $report = $this->reportUserActionTime
            ->where("created_at", '>=', $dateFrom)
            ->where("created_at", '<=' , $dateTo)
            ->first();
        $periodOfTimeWhenStatusUpdated = gmdate(
            "H:i:s",
            strtotime($userDataAfterBeingUpdated->getUpdatedAt()) - strtotime($userDataBeforeBeingUpdated->getUpdatedAt()),
        );

        if($report) {
            $statusColumnName = $userDataBeforeBeingUpdated->getStatusColumnName();

            return $report->update([
                 $statusColumnName => date(
                    "H:i:s",
                     strtotime($report->{$statusColumnName}) + strtotime($periodOfTimeWhenStatusUpdated) - strtotime('00:00:00'),
                )
            ]);

        }

        $this->reportUserActionTime->create([
            'user_id' => $userId
        ]);

        return true;

    }
}
