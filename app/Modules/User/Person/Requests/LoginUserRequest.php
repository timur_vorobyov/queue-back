<?php

namespace App\Modules\User\Person\Requests;

class LoginUserRequest extends AUserRequest
{
    public function __construct(string $email, string $password)
    {
        $this->password = $password;
        $this->email = $email;
    }

    public function toArray(): array
    {
        return [
            "email" => $this->email,
            "password" => $this->password,
        ];
    }

    public function validationRules(): array
    {
        return [
            "email" => "required|email",
            "password" => "required|string",
        ];
    }
}
