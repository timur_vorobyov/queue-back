<?php


namespace App\Modules\User\Person\Requests;


class UpdateUserStatusRequest extends AUserRequest
{

    public function __construct(
        int $userId,
        int $statusId
    ) {
        $this->userId = $userId;
        $this->statusId = $statusId;
    }

    public function toArray(): array
    {
        return [
            "userId" => $this->userId,
            "statusId" => $this->statusId
        ];
    }

    public function validationRules(): array
    {
        return [
            "userId" => "required",
            "statusId" => "required"
        ];
    }
}
