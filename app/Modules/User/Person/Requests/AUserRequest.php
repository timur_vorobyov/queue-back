<?php

namespace App\Modules\User\Person\Requests;

use App\Requests\Person\ARequest;

abstract class AUserRequest extends ARequest
{
    protected int $userId;
    protected int $statusId;
    protected ?int $userIdWithSameDeskNumber;
    protected int $branchOfficeId;
    protected int $townId;
    protected string $firstName;
    protected string $lastName;
    protected string $password;
    protected string $email;
    protected string $deskNumber;
    protected bool $isActive;
    protected int $visitPurposeId;
    protected array $visitPurposesIds;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getUserStatusId(): int
    {
        return $this->statusId;
    }

    public function getUserSettingsData(): array
    {
        return [
            "userId" => $this->userId,
            "branchOfficeId" => $this->branchOfficeId,
            "visitPurposesIds" => $this->visitPurposesIds,
            "deskNumber" => $this->deskNumber,
            "userIdWithSameDeskNumber" => $this->userIdWithSameDeskNumber,
        ];
    }
}
