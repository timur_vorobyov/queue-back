<?php

namespace App\Modules\BranchOffice\Person;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\BranchOffice\Person\Interfaces\IBranchOfficeRepository;
use App\Modules\Town\Town;

class BranchOfficeRepository implements IBranchOfficeRepository
{
    private Town $town;
    private BranchOffice $branchOffice;

    public function __construct()
    {
        $this->town = app(Town::class);
        $this->branchOffice = app(BranchOffice::class);
    }

    public function getBranchOfficesFilteredByTownId($townId): object
    {
        $branchOffices = $this->branchOffice::with('visitPurposes', 'town')->where("town_id", $townId)->get();
        return $branchOffices->map(function ($branchOffice) {
            return $branchOffice->toDomainEntity()->jsonSerialize();
        });
    }
}
