<?php

namespace App\Modules\BranchOffice\Person\Requests;

class ShowBranchOfficeRequest extends ABranchOfficeRequest
{
    public function __construct(int $townId)
    {
        $this->townId = $townId;
    }

    public function toArray(): array
    {
        return [
            "townId" => $this->townId,
        ];
    }

    public function validationRules(): array
    {
        return [
            "townId" => "required",
        ];
    }
}
