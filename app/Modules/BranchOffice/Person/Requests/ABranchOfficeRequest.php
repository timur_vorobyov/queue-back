<?php

namespace App\Modules\BranchOffice\Person\Requests;

use App\Requests\Person\ARequest;

abstract class ABranchOfficeRequest extends ARequest
{
    protected int $townId;

    public function getTownId(): int
    {
        return $this->townId;
    }
}
