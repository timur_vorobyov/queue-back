<?php


namespace App\Modules\BranchOffice\Person\Requests;


use Illuminate\Foundation\Http\FormRequest;


class AssociateBranchOfficesRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => 'required|numeric',
            'crmBranchName' => 'required|string',
            'displayName' => 'required|string'
        ];

        return $rules;
    }
}
