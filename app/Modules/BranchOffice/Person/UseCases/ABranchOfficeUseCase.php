<?php

namespace App\Modules\BranchOffice\Person\UseCases;

use App\Interfaces\IUseCase;
use App\Modules\BranchOffice\Person\Interfaces\IBranchOfficeRepository;

abstract class ABranchOfficeUseCase implements IUseCase
{
    protected IBranchOfficeRepository $townRepository;

    public function __construct(IBranchOfficeRepository $townRepository)
    {
        $this->townRepository = $townRepository;
    }
}
