<?php

namespace App\Modules\BranchOffice\Person\UseCases;

use App\Requests\Person\ARequest;

class ShowBranchOfficesUseCase extends ABranchOfficeUseCase
{
    public function perform(ARequest $request): object
    {
        $request->validate();

        return $this->townRepository->getBranchOfficesFilteredByTownId(
            $request->getTownId(),
        );
    }
}
