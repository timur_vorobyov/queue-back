<?php

namespace App\Modules\Ticket\Admin\SuperAdmin;

use App\Modules\Status\Status;
use App\Modules\Ticket\Ticket;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;
use App\Modules\Reports\ReportTicket\ReportTicket;


class TicketsExport implements FromCollection, WithHeadings, WithEvents, ShouldAutoSize
{
    use Exportable;

    private Status $status;
    private ?int $branchOfficeId;
    private ?int $visitPurposeId;
    private ?int $userId;
    private ?string $searchExpression;
    private ?string $dateFrom;
    private ?string $dateTo;

    public function __construct(array $filterData)
    {
        $this->dateFrom = $filterData["dateFrom"];
        $this->dateTo = $filterData["dateTo"];
        $this->branchOfficeId = $filterData["branchOfficeId"];
        $this->visitPurposeId = $filterData["visitPurposeId"];
        $this->userId = $filterData["userId"];
        $this->searchExpression = $filterData["searchExpression"];
        $this->status = app(Status::class);
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $dateFrom = $this->dateFrom == "1970-01-01" ? null : date('Y-m-d' , strtotime($this->dateFrom)).' 00:00:00';
        $dateTo = $this->dateTo == "1970-01-01" ? null : date('Y-m-d' , strtotime($this->dateTo)).' 23:59:59';
        $completedStatusId = $this->status::where('name', 'completed')->first()->id;
        $declinedStatusId = $this->status::where('name', 'declined')->first()->id;

        $ticketsQuery = Ticket::query()->with('visitPurpose', 'branchOffice', 'user', 'service')
            ->whereIn('status_id', [$completedStatusId, $declinedStatusId]);

        if ($this->branchOfficeId) {
            $ticketsQuery->where("branch_office_id", $this->branchOfficeId);
        }

        if($dateFrom && $dateTo) {
            $ticketsQuery
                ->where("created_at", '>=', $dateFrom)
                ->where("created_at", '<=' , $dateTo);
        }

        if ($this->visitPurposeId) {
            $ticketsQuery->where("visit_purpose_id", $this->visitPurposeId);
        }

        if ($this->userId) {
            $ticketsQuery->where("user_id", $this->userId);
        }

        if ($this->searchExpression) {
            $ticketsQuery
                ->where("first_name", "LIKE", "%$this->searchExpression%")
                ->orWhere("last_name", "LIKE", "%$this->searchExpression%")
                ->orWhere("email", "LIKE", "%$this->searchExpression%");
        }

        $ticketsIds = $ticketsQuery->orderBy("created_at", "asc")
            ->get()->pluck('id')->all();
        $modifiedTicketsId = '(' . implode(',', $ticketsIds) .')';

        return ReportTicket::
            whereRaw("ticket_id IN $modifiedTicketsId")
            ->get()
            ->map(function ($ticket) {
                return $ticket->toDomainEntity()->exportJsonSerialize();
            });
    }

    public function headings(): array
    {
        return [
            '#',
            'Полный номер билета',
            'Цель визита',
            'Услуга',
            'Номер телефона клиента',
            'Id клиента в CRM',
            'Дата создания',
            'Время создания',
            'Время ожидания',
            'Время обслужвания',
            'Имя юзера',
            'Филилал',
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        return [
            AfterSheet::class => function(AfterSheet $event) {
            $event->sheet->styleCells(
                'A1:L1',
                [
                'font' => [
                    'bold' => true
                ]
            ]);
            }
        ];
    }
}
