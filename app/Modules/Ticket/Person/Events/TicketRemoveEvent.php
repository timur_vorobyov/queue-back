<?php

namespace App\Modules\Ticket\Person\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TicketRemoveEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private string $status;
    private string|int $recipient;

    public function __construct(string $status, string|int $recipient)
    {
        $this->status = $status;
        $this->recipient = $recipient;
    }

    public function broadcastAs()
    {
        return "ticket-{$this->status}";
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("ticket.{$this->recipient}.{$this->status}.delete");
    }
}
