<?php

namespace App\Modules\Ticket\Person\Events;

use App\Modules\Ticket\ETicket;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TicketServiceEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private ETicket $ticket;
    private ?int $userId;
    private string $status;

    public function __construct(ETicket $ticket, string $status)
    {
        $this->ticket = $ticket;
        $this->status = $status;
        $this->userId = $ticket->getUserId();
    }

    public function broadcastAs()
    {
        return "ticket-serving";
    }

    public function broadcastWith()
    {
        return [
            "ticketServing" => $this->ticket->jsonSerialize(),
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("ticket.{$this->userId}.serving.{$this->status}");
    }
}
