<?php

namespace App\Modules\Ticket\Person\Events;

use App\Modules\Ticket\ETicket;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TicketPendingEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private ETicket $ticket;
    private ?int $visitPurposeId;
    private string $status;

    public function __construct(
        ETicket $ticket,
        ?int $visitPurposeId,
        string $status
    ) {
        $this->ticket = $ticket;
        $this->visitPurposeId = $visitPurposeId;
        $this->status = $status;
    }

    public function broadcastAs()
    {
        return "ticket-pending";
    }

    public function broadcastWith()
    {
        return [
            "ticketPending" => $this->ticket->jsonSerialize(),
            "shortTicketPending" => $this->ticket->jsonSerializeShortTicket(),
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel(
                "ticket.{$this->ticket->getBranchOfficeId()}.{$this->visitPurposeId}.pending.{$this->status}",
            );
    }
}
