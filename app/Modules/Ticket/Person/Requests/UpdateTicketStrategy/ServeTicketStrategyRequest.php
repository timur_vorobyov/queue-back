<?php

namespace App\Modules\Ticket\Person\Requests\UpdateTicketStrategy;

use App\Modules\Status\Status;

class ServeTicketStrategyRequest extends AUpdateTicketStrategyRequest
{
    public function __construct(int $ticketId, int $userId)
    {
        $this->ticketId = $ticketId;
        $this->userId = $userId;
        $this->desiredStatusName = Status::$servingStatus;
        $this->period = "served_at";
    }

    public function toArray(): array
    {
        return [
            "ticketId" => $this->ticketId,
        ];
    }

    public function validationRules(): array
    {
        return [
            "ticketId" => "required",
        ];
    }

    public function getDataToUpdateTicket(): array
    {
        return [
            "ticketId" => $this->ticketId,
            "desiredStatusName" => $this->desiredStatusName,
            "period" => $this->period,
        ];
    }
}
