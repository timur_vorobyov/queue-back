<?php

namespace App\Modules\Ticket\Person\Requests\UpdateTicketStrategy;

use App\Modules\Status\Status;

class CompleteTicketStrategyRequest extends AUpdateTicketStrategyRequest
{
    public function __construct(
        int $userId,
        int $ticketId,
        int $serviceId,
        int $visitPurposeId
    ) {
        $this->visitPurposeId =$visitPurposeId;
        $this->userId = $userId;
        $this->ticketId = $ticketId;
        $this->serviceId = $serviceId;
        $this->desiredStatusName = Status::$completedStatus;
        $this->period = "completed_at";
    }

    public function toArray(): array
    {
        return [
            "userId" => $this->userId,
            "ticketId" => $this->ticketId,
            "serviceId" => $this->serviceId,
            "visitPurposeId" => $this->visitPurposeId,
        ];
    }

    public function validationRules(): array
    {
        return [
            "userId" => "required",
            "ticketId" => "required",
            "serviceId" => "required",
            "visitPurposeId" => "required",
        ];
    }

    public function getDataToUpdateTicket(): array
    {
        return [
            "visitPurposeId" => $this->visitPurposeId,
            "ticketId" => $this->ticketId,
            "serviceId" => $this->serviceId,
            "desiredStatusName" => $this->desiredStatusName,
            "period" => $this->period,
        ];
    }
}
