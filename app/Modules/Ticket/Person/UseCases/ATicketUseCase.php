<?php

namespace App\Modules\Ticket\Person\UseCases;

use App\Interfaces\IUseCase;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;

abstract class ATicketUseCase implements IUseCase
{
    protected ITicketRepository $ticketRepository;

    public function __construct(ITicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }
}
