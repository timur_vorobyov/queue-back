<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\ETicket;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\Events\TicketServiceEvent;

class CompleteTicketStrategy implements IUpdateTicketStrategy
{
    public function runEvents(ETicket $pureTicketObject): string
    {
        event(new TicketServiceEvent($pureTicketObject, "delete"));

        return "Service is completed";
    }
}
