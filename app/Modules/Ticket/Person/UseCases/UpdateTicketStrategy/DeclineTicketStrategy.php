<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\ETicket;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;

class DeclineTicketStrategy implements IUpdateTicketStrategy
{
    public function runEvents(ETicket $pureTicketObject): string
    {
        event(
            new TicketAcceptEvent(
                $pureTicketObject,
                $pureTicketObject->getUserId(),
                "delete",
            ),
        );
        event(new TicketAcceptEvent($pureTicketObject, "all", "delete"));

        return "Client is declined";
    }
}
