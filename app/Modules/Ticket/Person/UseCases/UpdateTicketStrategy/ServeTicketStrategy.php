<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\ETicket;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\Events\TicketServiceEvent;

class ServeTicketStrategy implements IUpdateTicketStrategy
{
    public function runEvents(ETicket $pureTicketObject): string
    {
        event(
            new TicketAcceptEvent(
                $pureTicketObject,
                $pureTicketObject->getUserId(),
                "delete",
            ),
        );
        event(new TicketAcceptEvent($pureTicketObject, "all", "delete"));
        event(new TicketServiceEvent($pureTicketObject, "add"));

        return "Client is serving";
    }
}
