<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\ETicket;
use App\Modules\Ticket\Person\Events\TicketPendingEvent;
use App\Modules\Ticket\Person\Events\TicketAcceptEvent;
use App\Modules\Ticket\Person\Jobs\CallSenderJob;


class AcceptTicketStrategy implements IUpdateTicketStrategy
{
    public function runEvents(ETicket $pureTicketObject): string
    {
        event(new TicketPendingEvent($pureTicketObject, $pureTicketObject->getVisitPurposeId(), "delete"));
        event(
            new TicketAcceptEvent(
                $pureTicketObject,
                $pureTicketObject->getUserId(),
                "add",
            ),
        );
        event(new TicketAcceptEvent($pureTicketObject, "all", "add"));
        CallSenderJob::dispatch($pureTicketObject->getId())->delay(now()->addSeconds(120))->onQueue('calls');
        return "You have accepted client";
    }
}
