<?php

namespace App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy;

use App\Modules\Ticket\ETicket;

interface IUpdateTicketStrategy
{
    public function runEvents(ETicket $pureObject): string;
}
