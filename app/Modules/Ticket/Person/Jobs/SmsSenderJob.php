<?php

namespace App\Modules\Ticket\Person\Jobs;

use App\Services\SmsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SmsSenderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $customerPhoneNumber;
    private string $message;

    public function __construct(string $customerPhoneNumber, string $message)
    {
        $this->customerPhoneNumber = $customerPhoneNumber;
        $this->message = $message;
    }

    public function handle(SmsService $smsService)
    {
        $smsService->send($this->customerPhoneNumber, $this->message);
    }
}
