<?php

namespace App\Modules\Ticket\Person;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Reports\ReportTicket\ReportTicket;
use App\Modules\Service\Service;
use App\Modules\Status\Status;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use App\Modules\Ticket\Ticket;
use App\Modules\User\User;
use App\Modules\Ticket\ETicket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class TicketRepository implements ITicketRepository
{
    private Ticket $ticket;
    private User $user;
    private Status $status;
    private Service $service;
    private int $ticketNumber;
    private BranchOffice $branchOffice;

    public function __construct()
    {
        $this->ticket = app(Ticket::class);
        $this->user = app(User::class);
        $this->status = app(Status::class);
        $this->service = app(Service::class);
        $this->branchOffice = app(BranchOffice::class);
    }

    public function saveTicket(array $createTicketData): object
    {
        $status = $this->status
            ::where("name", $createTicketData["currentStatusName"])
            ->first();

        $ticket = $this->ticket
            ::create([
                "visit_purpose_id" => $createTicketData["visitPurposeId"],
                "branch_office_id" => $createTicketData["branchOfficeId"],
                "status_id" => $status->id,
                "service_id" => null,
                "user_id" => null,
                "ticket_number" => $this->getTicketNumber(
                    $createTicketData["visitPurposeId"],
                    $createTicketData["branchOfficeId"],
                ),
                "customer_phone_number" =>
                    $createTicketData["customerPhoneNumber"],
                "is_salom_cache" =>
                    isset($createTicketData["isSalomCache"]) ?? false,
            ]);

        $savedTicket = $this->ticket::with('branchOffice', 'service', 'visitPurpose')
            ->find($ticket->id);

        $pureSavedTicket = $savedTicket->toDomainEntity();

        ReportTicket::create([
            "ticket_id" => $pureSavedTicket->getId(),
            "ticket_full_number" => $pureSavedTicket->getFullTicketNumberWithoutDash(),
            "visit_purpose_name" =>$pureSavedTicket->getEVisitPurpose()->getName(),
            "service_name" => $pureSavedTicket->getEService() ? $pureSavedTicket->getEService()->getName() : '',
            "customer_phone_number" => $pureSavedTicket->getCustomerPhoneNumber(),
            "date_created_at" => date("Y-m-d", strtotime($pureSavedTicket->getCreatedAt())),
            "time_created_at" => date("H:i:s", strtotime($pureSavedTicket->getCreatedAt())),
            "branch_office_name" => $pureSavedTicket->getBranchOfficeFullName(),
            "customer_service_time" => "Клиент не подошёл",
            "customer_pending_time" => "Клиента не пригласили"
        ]);

        return $savedTicket;
    }

    private function getTicketNumber(
        int $visitPurposeId,
        int $branchOfficeId
    ): int {
        $this->setTicketNumber($visitPurposeId, $branchOfficeId);
        return $this->ticketNumber;
    }

    private function setTicketNumber(
        int $visitPurposeId,
        int $branchOfficeId
    ): void {
        $dateToday = Carbon::now()->toDateString();

        $ticketNumber = $this->ticket
            ::where([
                ["visit_purpose_id", $visitPurposeId],
                ["created_at", ">=", $dateToday],
                ["branch_office_id", $branchOfficeId],
            ])
            ->max("ticket_number");

        if (!$ticketNumber) {
            $ticketNumber = 0;
        }

        $this->ticketNumber = $ticketNumber + 1;
    }

    public function getTicket(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposesIds,
        int $userId
    ): array | null
    {
        $currentStatus = $this->status
            ::where("name", $statusName)
            ->first();
        $ticket = $this->ticket
            ::with("visitPurpose", "branchOffice")
            ->where("branch_office_id", $branchOfficeId)
            ->where("user_id", $userId)
            ->whereIn("visit_purpose_id", $visitPurposesIds)
            ->where("status_id", $currentStatus->id)
            ->first();

        if ($ticket) {
            return  $ticket->toDomainEntity()->jsonSerialize();
        }

        return null;
    }

    public function getFullTicketsList(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposesIds
    ): object {
        $currentStatus = $this->status
            ::where("name", $statusName)
            ->first();
        $tickets = $this->ticket
            ::with("visitPurpose", "branchOffice")
            ->where("branch_office_id", $branchOfficeId)
            ->whereIn("visit_purpose_id", $visitPurposesIds)
            ->where("status_id", $currentStatus->id)
            ->get();

        $salomCacheTickets = $this->getSalomCacheTickets($visitPurposesIds, $branchOfficeId, $currentStatus->id);

        if ($salomCacheTickets->isNotEmpty()) {
            $tickets = $salomCacheTickets->merge($tickets);
        }

        return $tickets->map(function ($ticket) {
            return !empty($ticket) ? $ticket->toDomainEntity()->jsonSerialize() : $ticket;
        });
    }

    public function getShortTicketsList(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposeIds
    ): object {
        $currentStatus = $this->status
            ::where("name", $statusName)
            ->first();
        $tickets = $this->ticket
            ::with("user", "visitPurpose", "branchOffice")
            ->where("branch_office_id", $branchOfficeId)
            ->where("status_id", $currentStatus->id)
            ->whereIn("visit_purpose_id", $visitPurposeIds)
            ->get();

        //
        $salomCacheTickets = $this->getSalomCacheTickets($visitPurposeIds, $branchOfficeId, $currentStatus->id);

        if ($salomCacheTickets->isNotEmpty()) {
            $tickets = $salomCacheTickets->merge($tickets);
        }

        return $tickets->map(function ($ticket) {
            return $ticket->toDomainEntity()->jsonSerializeShortTicket();
        });
    }

/**
 * @param array $visitPurposeIds
 * @param int $branchOfficeId
 * @param int $currentStatusId
 * @return mixed
 */
    private function getSalomCacheTickets(array $visitPurposeIds, int $branchOfficeId, int $currentStatusId): Collection|null
    {
        return $this->ticket
            ::with("user", "visitPurpose", "branchOffice")
            ->whereIn("visit_purpose_id", $visitPurposeIds)
            ->where("branch_office_id", $branchOfficeId)
            ->where("status_id", $currentStatusId)
            ->where("is_salom_cache", true)
            ->get();
    }

    public function updateTicket(array $attributes): int
    {
        $desiredStatus = $this->status
            ::where("name", $attributes["desiredStatusName"])
            ->first();

        switch($attributes["desiredStatusName"]) {
            case 'invited':
                $currentStatus = $this->status
                    ::where("name", Status::$newStatus)
                    ->first();
                $user = $this->user::find($attributes["userId"]);
                $visitPurposesIds = $user->visitPurposes()->pluck('visit_purpose_id')->all();
                $ticket = $this->ticket
                    ::with("visitPurpose", "branchOffice")
                    ->whereIn("visit_purpose_id", $visitPurposesIds)
                    ->where("status_id", $currentStatus->id)
                    ->where("branch_office_id", $user->toDomainEntity()->getBranchOfficeId())
                    ->first();

                $salomCacheTicket = $this->ticket
                    ::with("visitPurpose", "branchOffice")
                    ->whereIn("visit_purpose_id", $visitPurposesIds)
                    ->where("status_id", $currentStatus->id)
                    ->where("branch_office_id", $user->toDomainEntity()->getBranchOfficeId())
                    ->where("is_salom_cache", true)
                    ->first();

                if ($salomCacheTicket) {
                    $ticket = $salomCacheTicket;
                }

                $isUpdated = $ticket->update([
                    "user_id" => $attributes["userId"],
                    "status_id" => $desiredStatus->id,
                    $attributes["period"] => Carbon::now()
                ]);
                ReportTicket::where('ticket_id', $ticket->id)
                    ->update([
                        "user_first_name" => $ticket->user->toDomainEntity()->getFirstName(),
                        "crm_customer_id" => $ticket->toDomainEntity()->getCrmCustomerId(),
                        "customer_pending_time" => gmdate(
                            "H:i:s",
                            strtotime($ticket->toDomainEntity()->getInvitedAt()) - strtotime($ticket->toDomainEntity()->getCreatedAt())
                        )
                    ]);
                break;
            case 'completed':
                $ticket = $this->ticket::find($attributes["ticketId"]);
                $service = $this->service::find($attributes["serviceId"]);
                $isUpdated = $ticket->update([
                    "status_id" => $desiredStatus->id,
                    "service_id" => $service->id,
                    $attributes["period"] => Carbon::now(),
                    "visit_purpose_id" => $attributes["visitPurposeId"]
                ]);
                ReportTicket::where('ticket_id', $ticket->id)
                    ->update([
                        "service_name" => $service->name,
                        "customer_service_time" => gmdate(
                            "H:i:s",
                            strtotime($ticket->toDomainEntity()->getCompletedAt()) - strtotime($ticket->toDomainEntity()->getInvitedAt())
                        )
                    ]);
                break;
            default:
                $ticket = $this->ticket::find($attributes["ticketId"]);
                $isUpdated = $ticket->update([
                    "status_id" => $desiredStatus->id,
                    $attributes["period"] => Carbon::now()
                ]);
                break;
        }

        if ($isUpdated) {
            return $ticket->id;
        }

        return 0;
    }

    public function getUserVisitPurposesCount(int $userId): int
    {
        $user = $this->user->find($userId);

        $attachedVisitPurposesIdsCount = count($user->visitPurposes()->pluck('visit_purpose_id')->all());

        if ($attachedVisitPurposesIdsCount) {
            return 1;
        }

        return 0;
    }

    public function isUserDeskNumberNull(int $userId): bool
    {
        $deskNumber = $this->user
            ::find($userId)
            ->toDomainEntity()
            ->getDeskNumber();
        if (!isset($deskNumber)) {
            return true;
        }

        return false;
    }

    public function getTicketFilteredById(
        int $ticketId
    ): ETicket {
        return $this->ticket
            ::with("user", "visitPurpose", "branchOffice")
            ->find($ticketId)
            ->toDomainEntity();
    }

    public function updateTicketReport(ETicket $pureTicketObject): void
    {
        $ticketReport = ReportTicket::where('ticket_id',$pureTicketObject->getId())->first();
        $ticketReport->customer_service_time = $pureTicketObject->getCompletedAt() ? date(
            "H:i:s",
            strtotime($pureTicketObject->getCompletedAt()) - strtotime($pureTicketObject->getInvitedAt()),
        ) : 'Клиент не подошёл';
        $ticketReport->service_name = $pureTicketObject->getEService() ? $pureTicketObject->getEService()->getName() : '';
        $ticketReport->user_first_name = $pureTicketObject->getUser() ? $pureTicketObject->getUser()->getFirstName() : '';
        $ticketReport->crm_customer_id = $pureTicketObject->getCrmCustomerId();
        $ticketReport->customer_pending_time = $pureTicketObject->getInvitedAt() ? date(
            "H:i:s",
            strtotime($pureTicketObject->getInvitedAt()) - strtotime($pureTicketObject->getCreatedAt()),
        ): 'Клиент не был приглашён';

        $ticketReport->save();
    }

    /**
    * @param array $params
    * @return bool
    */
    public function associateBranches(array $params): bool
    {
        try {
            $branchOffice = $this->branchOffice::find($params['id']);
            $branchOffice->crm_branch_name = $params["crmBranchName"];
            $branchOffice->display_name = $params["displayName"];

            return $branchOffice->save();

        } catch (\Exception $e) {
            return false;
        }
    }

}
