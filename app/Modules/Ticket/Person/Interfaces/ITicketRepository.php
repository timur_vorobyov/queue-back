<?php

namespace App\Modules\Ticket\Person\Interfaces;

use App\Modules\Ticket\ETicket;

interface ITicketRepository
{
    public function saveTicket(array $createTicketData): object;
    public function getShortTicketsList(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposeIds
    ): object;
    public function getTicket(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposesIds,
        int $userId
    ): array | null;
    public function getFullTicketsList(
        int $branchOfficeId,
        string $statusName,
        array $visitPurposesIds
    ): object;
    public function isUserDeskNumberNull(int $userId): bool;
    public function getUserVisitPurposesCount(int $userId): int;
    public function updateTicket(array $dataToUpdate): int;
    public function getTicketFilteredById(
        int $ticketId
    ): ETicket;
    public function associateBranches(array $params);
}
