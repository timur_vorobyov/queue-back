<?php

namespace App\Modules\Town;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Office\Office;
use Database\Factories\TownFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function newFactory()
    {
        return TownFactory::new();
    }

    public function toDomainEntity(): ETown
    {
        return new ETown($this->id, $this->name);
    }
}
