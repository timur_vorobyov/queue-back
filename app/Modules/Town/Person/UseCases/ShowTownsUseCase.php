<?php

namespace App\Modules\Town\Person\UseCases;

class ShowTownsUseCase extends ATownUseCase
{
    public function perform(): array
    {
        return $this->townRepository->getAllTowns();
    }
}
