<?php

namespace App\Modules\Town\Person;

use App\Modules\Town\Person\Interfaces\ITownRepository;
use App\Modules\Town\Town;

class TownRepository implements ITownRepository
{
    private Town $town;

    public function __construct()
    {
        $this->town = app(Town::class);
    }

    public function getAllTowns(): array
    {
        return $this->town::all()->toArray();
    }
}
