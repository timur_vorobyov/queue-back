<?php

namespace App\Modules\Town\Person\Interfaces;

interface ITownRepository
{
    public function getAllTowns(): array;
}
