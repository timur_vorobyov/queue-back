<?php

namespace App\Modules\Service;

use App\Modules\BranchOffice\BranchOffice;
use App\Modules\VisitPurpose\VisitPurpose;
use Database\Factories\ServiceFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];

    use HasFactory;

    protected static function newFactory()
    {
        return ServiceFactory::new();
    }

    public function visitPurposes()
    {
        return $this->belongsToMany(VisitPurpose::class);
    }

    public function toDomainEntity(): EService
    {
        return new EService(
            $this->id,
            $this->name,
            $this->is_active,
        );
    }
}
