<?php

namespace App\Modules\Service;

use App\Interfaces\IJsonSerializable;
use App\Modules\VisitPurpose\EVisitPurpose;

class EService implements IJsonSerializable
{
    private int $id;
    private string $name;
    private bool $isActive;

    public function __construct(
        int $id,
        string $name,
        bool $isActive
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "isActive" => $this->isActive,
        ];
    }

    public function superAdminJsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "isActive" => $this->isActive,
        ];
    }

    public function adminJsonSerialize(): array
    {
        // TODO: Implement adminJsonSerialize() method.
    }

    public function getName(): string
    {
        return $this->name;
    }
}
