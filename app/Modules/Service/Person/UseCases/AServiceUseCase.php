<?php

namespace App\Modules\Service\Person\UseCases;

use App\Interfaces\IUseCase;
use App\Modules\Service\Person\Interfaces\IServiceRepository;
use App\Requests\Person\ARequest;

abstract class AServiceUseCase implements IUseCase
{
    protected IServiceRepository $serviceRepository;

    public function __construct(IServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }
}
