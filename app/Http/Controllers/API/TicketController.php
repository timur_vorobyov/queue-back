<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Modules\BranchOffice\BranchOffice;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use App\Modules\Ticket\Person\Requests\CRMStoreTicketRequest;
use App\Modules\Ticket\Person\UseCases\StoreTicketUseCase;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    private ITicketRepository $ticketRepository;

    public function __construct()
    {
        $this->ticketRepository = app(ITicketRepository::class);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $branchOffice = BranchOffice::where('crm_branch_name', $request->crmAlifBranchName)->first();

        if ($branchOffice) {
            $useCaseRequest = new CRMStoreTicketRequest(
                $request->visitPurposeId,
                $request->customerPhoneNumber,
                $branchOffice->id,
            );

            $useCase = app(StoreTicketUseCase::class);
            $ticket = $useCase->perform($useCaseRequest);

            return response()->json([
                "result" => empty($ticket) ? false : true,
                "ticket" => $ticket,
            ]);
        }

            return response()->json([
                "result" =>  false,
                "ticket" => 'branches are not associated!',
            ]);
    }
}
