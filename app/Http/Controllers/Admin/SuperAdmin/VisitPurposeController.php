<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VisitPurposeController extends Controller
{
    private VisitPurpose $visitPurpose;
    private Validator $validator;

    public function __construct()
    {
        $this->visitPurpose = app(VisitPurpose::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "servicesIds" => "required",
            "name" => "required",
            "series" => "required|unique:visit_purposes",
            "tjMessage" => "required",
            "ruMessage" => "required",
        ]);

        if($validator->fails()) {

            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The series has already been taken." ? "INITIAL_ERROR" : $message
            ]);
        }

        $visitPurpose = $this->visitPurpose::create([
            "name" => $request->name,
            "series" => $request->series,
            "tj_message" => $request->tjMessage,
            "ru_message" => $request->ruMessage,
        ]);

        $visitPurpose->services()->attach($request->servicesIds);

        return response()->json([
            "message" => $visitPurpose->id
                ? "Visit purpose is saved"
                : "Visit purpose is not saved",
        ]);
    }

    public function update(Request $request, VisitPurpose $visitPurpose)
    {
        $visitPurposeIsUpdated = $visitPurpose->update([
            "name" => $request->name,
            "series" => $request->series,
            "tj_message" => $request->tjMessage,
            "ru_message" => $request->ruMessage,
        ]);

        $visitPurpose->services()->sync($request->servicesIds);


        return response()->json([
            "message" => $visitPurposeIsUpdated
                ? "Visit purpose is updated"
                : "Visit purpose is not updated",
        ]);
    }

    public function getPaginatedVisitPurposesList(Request $request)
    {
        $branchOfficeId = $request->branchOfficeId;
        $searchExpression = $request->searchExpression;

        $visitPurposesQuery = $this->visitPurpose::with('branchOffices', 'services');

        if ($branchOfficeId) {
            $visitPurposesQuery->whereHas("branchOffices", function ($q) use ($branchOfficeId) {
                $q->where('branch_office_id', $branchOfficeId);
            });
        }

        if ($searchExpression) {
            $visitPurposesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $visitPurposesList = $visitPurposesQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $visitPurposesList->total(),
            "totalPages" => $visitPurposesList->lastPage(),
            "currentPage" => $visitPurposesList->currentPage(),
            "pageLimit" => $visitPurposesList->perPage(),
        ];

        $visitPurposesPureList = [];

        foreach ($visitPurposesList->items() as $visitPurpose) {
            array_push(
                $visitPurposesPureList,
                $visitPurpose->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "visitPurposesPureList" => $visitPurposesPureList,
        ]);
    }

    public function delete(VisitPurpose $visitPurpose)
    {
        $isRemoved = $visitPurpose->delete();
        return response()->json([
            "message" => $isRemoved
                ? "Visit purpose is removed"
                : "Visit purpose is not removed",
        ]);
    }
}
