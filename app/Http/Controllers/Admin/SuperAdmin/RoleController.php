<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Role\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    private Role $role;
    private Validator $validator;

    public function __construct()
    {
        $this->role = app(Role::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $role = $this->role::create([
            "name" => $request->name,
            "guard_name" => "web",
        ]);

        $role->syncPermissions($request->permissionIds);

        return response()->json([
            "message" => $role->id ? "Role is saved" : "Role is not saved",
        ]);
    }

    public function update(Request $request, Role $role)
    {
        $isUpdated = $role->update([
            "name" => $request->name,
        ]);

        $role->syncPermissions($request->permissionIds);

        return response()->json([
            "message" => $isUpdated ? "Role is updated" : "Role is not updated",
        ]);
    }

    public function getRolesList()
    {
        $rolesList = $this->role::all()->map(function ($role) {
            return $role->toDomainEntity()->superAdminJsonSerialize();
        });

        return response()->json($rolesList);
    }

    public function delete(Role $role)
    {
        $isRemoved = $role->delete();
        return response()->json([
            "message" => $isRemoved ? "Role is removed" : "Role is not removed",
        ]);
    }
}
