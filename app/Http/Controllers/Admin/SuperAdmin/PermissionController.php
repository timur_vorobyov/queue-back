<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Permission\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    private Permission $permission;
    private Validator $validator;

    public function __construct()
    {
        $this->permission = app(Permission::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $permission = $this->permission::create([
            "name" => $request->name,
            "guard_name" => "web",
        ]);

        return response()->json([
            "message" => $permission->id
                ? "Permission is saved"
                : "Permission is not saved",
        ]);
    }

    public function update(Request $request, Permission $permission)
    {
        $isUpdated = $permission->update([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $isUpdated
                ? "Permission is updated"
                : "Permission is not updated",
        ]);
    }

    public function getPermissionsList()
    {
        $permissionsList = $this->permission
            ::all()
            ->map(function ($permission) {
                return $permission->toDomainEntity()->superAdminJsonSerialize();
            });

        return response()->json($permissionsList);
    }

    public function delete(Permission $permission)
    {
        $isRemoved = $permission->delete();
        return response()->json([
            "message" => $isRemoved
                ? "Permission is removed"
                : "Permission is not removed",
        ]);
    }
}
