<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\BranchOffice\BranchOffice;
use App\Modules\BranchOffice\Person\Requests\AssociateBranchOfficesRequest;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchOfficeController extends Controller
{
    private BranchOffice $branchOffice;
    private Validator $validator;
    private ITicketRepository $ticketRepository;

    public function __construct()
    {
        $this->branchOffice = app(BranchOffice::class);
        $this->validator = app(Validator::class);
        $this->ticketRepository = app(ITicketRepository::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "initial" => "required|unique:branch_offices",
            "townId" => "required",
            "name" => "required",
            "timer" => "required",
            "visitPurposesIds" => "required",
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();

            return response()->json([
                "statusCode" => 422,
                "message" => $message == "The initial has already been taken." ? "INITIAL_ERROR" : $message,
            ]);
        }

        $branchOffice = $this->branchOffice::create([
            "initial" => $request->initial,
            "town_id" => $request->townId,
            "name" => $request->name,
            "timer" => $request->timer,
        ]);

        $branchOffice->visitPurposes()->attach($request->visitPurposesIds);

        $branchOfficeId = $branchOffice->id;

        if ($branchOfficeId && isset($request->crmBranchName)) {
            $params = $request->all();
            $params["id"] = $branchOfficeId;
            $this->ticketRepository->associateBranches($params);
        }

        return response()->json([
            "message" => $branchOfficeId
                ? "Branch office is saved"
                : "Branch office is not saved",
        ]);
    }

    public function update(Request $request, BranchOffice $branchOffice)
    {
        $isUpdated = $branchOffice->update([
            "town_id" => $request->townId,
            "name" => $request->name,
            "initial" => $request->initial,
            "timer" => $request->timer,
        ]);

        $branchOffice->visitPurposes()->sync($request->visitPurposesIds);

        return response()->json([
            "message" => $isUpdated
                ? "Branch office is updated"
                : "Branch office is not updated",
        ]);
    }

    public function getBranchOfficesList()
    {
        $branchOfficesList = $this->branchOffice
            ::with('visitPurposes', 'town')
            ->get()
            ->map(function ($branchOffice) {
                return $branchOffice
                    ->toDomainEntity()
                    ->superAdminJsonSerialize();
            });

        return response()->json($branchOfficesList);
    }

    public function delete(BranchOffice $branchOffice)
    {
        $isRemoved = $branchOffice->delete();
        return response()->json([
            "message" => $isRemoved
                ? "Branch office is removed"
                : "Branch office is not removed",
        ]);
    }

    /**
     * @param AssociateBranchOfficesRequest $request
     * @return JsonResponse
     */
    public function associateBranches(AssociateBranchOfficesRequest $request)
    {
        $areBranchesAssociated = $this->ticketRepository->associateBranches(
            $request->all(),
        );

        return response()->json([
            "message" => $areBranchesAssociated
                ? "BRANCHES_ARE_ASSOCIATED"
                : "BRANCHES_ARE_NOT_ASSOCIATED",
        ]);
    }
}
