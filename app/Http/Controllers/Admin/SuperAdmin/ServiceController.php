<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Service\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    private Service $service;
    private Validator $validator;

    public function __construct()
    {
        $this->service = app(Service::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required",
            "isActive" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $service = $this->service::create([
            "name" => $request->name,
            "is_active" => $request->isActive,
        ]);

        return response()->json([
            "message" => $service->id
                ? "Service is saved"
                : "Service is not saved",
        ]);
    }

    public function update(Request $request, Service $service)
    {
        $isUpdated = $service->update([
            "name" => $request->name,
            "is_active" => $request->isActive,
        ]);

        return response()->json([
            "message" => $isUpdated
                ? "Service is updated"
                : "Service is not updated",
        ]);
    }

    public function getServicesList(Request $request)
    {
        $visitPurposeId = $request->visitPurposeId;
        $searchExpression = $request->searchExpression;

        $servicesQuery = $this->service::query();

        if ($visitPurposeId) {
            $servicesQuery->whereHas("visitPurposes", function ($q) use ($visitPurposeId) {
                $q->where('visit_purpose_id', $visitPurposeId);
            });
        }

        if ($searchExpression) {
            $servicesQuery->where("name", "LIKE", "%$searchExpression%");
        }

        $servicesList = $servicesQuery->paginate($request->limit);
        $pagination = [
            "totalRecords" => $servicesList->total(),
            "totalPages" => $servicesList->lastPage(),
            "currentPage" => $servicesList->currentPage(),
            "pageLimit" => $servicesList->perPage(),
        ];

        $servicesPureList = [];

        foreach ($servicesList->items() as $service) {
            array_push(
                $servicesPureList,
                $service->toDomainEntity()->superAdminJsonSerialize(),
            );
        }

        return response()->json([
            "pagination" => $pagination,
            "servicesPureList" => $servicesPureList,
        ]);
    }

    public function delete(Service $service)
    {
        $isRemoved = $service->delete();
        return response()->json([
            "message" => $isRemoved
                ? "Service is removed"
                : "Service is not removed",
        ]);
    }
}
