<?php


namespace App\Http\Controllers\Admin\SuperAdmin;


use App\Http\Controllers\Controller;
use App\Modules\Ticket\Admin\SuperAdmin\TicketsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class TicketController extends Controller
{
    private Excel $excel;

    public function __construct(Excel $excel) {
        $this->excel = $excel;
    }

    public function export(Request $request)
    {
        return $this->excel->download(new TicketsExport($request->all()),'tickets.xlsx');
    }
}
