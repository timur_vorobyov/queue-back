<?php

namespace App\Http\Controllers\Admin\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Modules\Town\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TownController extends Controller
{
    private Town $town;
    private Validator $validator;

    public function __construct()
    {
        $this->town = app(Town::class);
        $this->validator = app(Validator::class);
    }

    public function store(Request $request)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required",
        ]);

        if($validator->fails()) {
            return response()->json([
                "statusCode" => 422,
            ]);
        }

        $town = $this->town::create([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $town->id ? "Town is saved" : "Town is not saved",
        ]);
    }

    public function update(Request $request, Town $town)
    {
        $isUpdated = $town->update([
            "name" => $request->name,
        ]);

        return response()->json([
            "message" => $isUpdated ? "Town is updated" : "Town is not updated",
        ]);
    }

    public function getTownsList()
    {
        $townsList = $this->town::all()->map(function ($town) {
            return $town->toDomainEntity()->superAdminJsonSerialize();
        });

        return response()->json($townsList);
    }

    public function delete(Town $town)
    {
        $isRemoved = $town->delete();
        return response()->json([
            "message" => $isRemoved ? "Town is removed" : "Town is not removed",
        ]);
    }
}
