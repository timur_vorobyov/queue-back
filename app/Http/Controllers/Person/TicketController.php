<?php

namespace App\Http\Controllers\Person;

use App\Http\Controllers\Controller;
use App\Modules\Ticket\Person\Interfaces\ITicketRepository;
use App\Modules\Ticket\Person\Requests\StoreTicketRequest;
use App\Modules\Ticket\Person\Requests\UpdateTicketStrategy\AcceptTicketStrategyRequest;
use App\Modules\Ticket\Person\Requests\UpdateTicketStrategy\CompleteTicketStrategyRequest;
use App\Modules\Ticket\Person\Requests\UpdateTicketStrategy\DeclineTicketStrategyRequest;
use App\Modules\Ticket\Person\Requests\UpdateTicketStrategy\ServeTicketStrategyRequest;
use App\Modules\Ticket\Person\UseCases\StoreTicketUseCase;
use App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy\AcceptTicketStrategy;
use App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy\CompleteTicketStrategy;
use App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy\DeclineTicketStrategy;
use App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy\ServeTicketStrategy;
use App\Modules\Ticket\Person\UseCases\UpdateTicketStrategy\UpdateTicketUseCase;
use App\Services\Interfaces\ICustomerSearchService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{

    private Validator $validator;
    private ITicketRepository $ticketRepository;
    private ICustomerSearchService $crmService;

    public function __construct()
    {
        $this->validator = app(Validator::class);
        $this->ticketRepository = app(ITicketRepository::class);
        $this->crmService = app(ICustomerSearchService::class);
    }

    public function store(Request $request)
    {
        $useCaseRequest = new StoreTicketRequest(
            $request->visitPurposeId,
            $request->customerPhoneNumber,
            $request->branchOfficeId,
        );

        $useCase = app(StoreTicketUseCase::class);
        $ticket = $useCase->perform($useCaseRequest);

        return response()->json([
            "ticket" => $ticket,
        ]);
    }

    public function acceptTicket(Request $request)
    {
        $useCaseRequest = new AcceptTicketStrategyRequest(
            $request->userId,
        );

        $useCase = new UpdateTicketUseCase(new AcceptTicketStrategy());
        $message = $useCase->perform($useCaseRequest);

        return response()->json([
            "data" => $message,
        ]);
    }

    public function serveTicket(Request $request)
    {
        $useCaseRequest = new ServeTicketStrategyRequest(
            $request->ticketId,
            $request->userId
        );

        $useCase = new UpdateTicketUseCase(new ServeTicketStrategy());
        $message = $useCase->perform($useCaseRequest);

        return response()->json([
            "data" => $message,
        ]);
    }

    public function declineTicket(Request $request)
    {
        $useCaseRequest = new DeclineTicketStrategyRequest(
            $request->ticketId,
            $request->userId
        );

        $useCase = new UpdateTicketUseCase(new DeclineTicketStrategy());
        $message = $useCase->perform($useCaseRequest);

        return response()->json([
            "data" => $message,
        ]);
    }

    public function completeTicket(Request $request)
    {
        $useCaseRequest = new CompleteTicketStrategyRequest(
            $request->userId,
            $request->ticketId,
            $request->serviceId,
            $request->visitPurposeId
        );

        $useCase = new UpdateTicketUseCase(new CompleteTicketStrategy());
        $message = $useCase->perform($useCaseRequest);

        return response()->json([
            "data" => $message,
        ]);
    }

    public function getFullTicketsList(Request $request)
    {
        $this->validator::make($request->all(), [
            "branchOfficeId" => "required",
            "statusName" => "required",
            "visitPurposesIds" => "required",
        ]);

        $tickets = $this->ticketRepository->getFullTicketsList(
            $request->branchOfficeId,
            $request->statusName,
            $request->visitPurposesIds
        );

        return response()->json($tickets);
    }

    public function getShortTicketsList(Request $request)
    {
        $this->validator::make($request->all(), [
            "branchOfficeId" => "required",
            "statusName" => "required",
            "visitPurposeIds" => "required",
        ]);

        $tickets = $this->ticketRepository->getShortTicketsList(
            $request->branchOfficeId,
            $request->statusName,
            $request->visitPurposeIds
        );

        return response()->json($tickets);
    }

    public function getTicket(Request $request)
    {
        $this->validator::make($request->all(), [
            "branchOfficeId" => "required",
            "statusName" => "required",
            "visitPurposesIds" => "required",
            "userId" => "required",
        ]);

        $ticket = $this->ticketRepository->getTicket(
            $request->branchOfficeId,
            $request->statusName,
            $request->visitPurposesIds,
            $request->userId
        );

        return response()->json($ticket);
    }

    /**
     * @return JsonResponse
     */
    public function getCRMBranches(): JsonResponse
    {
        return $this->crmService->getCRMBranches();
    }
}
