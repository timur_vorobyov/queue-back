<?php

namespace App\Http\Controllers\Person;

use App\Http\Controllers\Controller;
use App\Modules\User\Person\Events\UserStatisticsDataEvent;
use App\Modules\User\Person\Requests\LoginUserRequest;
use App\Modules\User\Person\Requests\UpdateUserSettingsRequest;
use App\Modules\User\Person\Requests\UpdateUserStatusRequest;
use App\Modules\User\Person\Service\StatisticsRecipientService;
use App\Modules\User\Person\UseCases\LoginUserUseCase;
use App\Modules\User\Person\UseCases\UpdateUserSettingsUseCase;
use App\Modules\User\Person\UseCases\UpdateUserStatusUseCase;
use App\Modules\User\Person\UserRepository;
use App\Modules\User\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private StatisticsRecipientService $dataRecipientService;
    private User $user;
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->dataRecipientService = app(StatisticsRecipientService::class);
        $this->user = app(User::class);
        $this->userRepository = app(UserRepository::class);
    }

    public function login(Request $request)
    {
        $useCaseRequest = new LoginUserRequest(
            $request->email,
            $request->password,
        );

        $useCase = app(LoginUserUseCase::class);
        $data = $useCase->perform($useCaseRequest);

        return response()->json([
            "message" => $data["token"]
                ? "You are logged in"
                : "We can`t find an account with this credentials",
            "accessToken" => $data["token"],
            "id" => $data["userId"],
        ]);
    }

    public function updateWorkData(Request $request)
    {
        $useCaseRequest = new UpdateUserSettingsRequest(
            $request->userId,
            $request->branchOfficeId,
            $request->visitPurposesIds,
            $request->deskNumber,
            $request->userIdWithSameDeskNumber,
        );

        $useCase = app(UpdateUserSettingsUseCase::class);
        $userData = $useCase->perform($useCaseRequest);

        return response()->json([
            "message" => array_key_exists('errorMessage', $userData) ? $userData['errorMessage'] : "User's settings are updated",
            "userData" => !array_key_exists('errorMessage', $userData) ? $userData : '',
        ]);
    }

    public function isDeskNumberFree(Request $request)
    {
        $user = $this->user::with('branchOffice')->find($request->userId)->toDomainEntity();
        $branchInitial = $user->getEBranchOffice()->getInitial();
        $fullDeskNumber = $request->deskNumber . $branchInitial;
        $countSavedDeskNumber = $this->userRepository->getDeskNumberExistingCount($fullDeskNumber);

        if($countSavedDeskNumber >= 1) {
            $usersWithSameDeskNumber = $this->userRepository->getUsersFilteredByDeskNumber($fullDeskNumber);

            foreach ($usersWithSameDeskNumber as $userWithSameDeskNumber) {
                if($user->getId() == $userWithSameDeskNumber->getId()) {
                    return response()->json([
                        "message" => "Update is allowed"
                    ]);
                }

                if(!$userWithSameDeskNumber->isAllowedHaveSameDeskNumber() ||
                    ($user->isAllowedHaveSameDeskNumber() && $userWithSameDeskNumber->isAllowedHaveSameDeskNumber())
                ) {
                    return response()->json([
                        "message" => "Update is not allowed",
                        'userFirstName' => $userWithSameDeskNumber->getFirstName(),
                        'userIdWithSameDeskNumber' => $userWithSameDeskNumber->getId()
                    ]);
                }
            }

            return response()->json([
                "message" => "Update is allowed"
            ]);
        }

        return response()->json([
            "message" => "Update is allowed"
        ]);
    }

    public function getUsersAverageServiceTime(Request $request)
    {
        $usersAverageServiceTime = $this->dataRecipientService->getUsersAverageServiceTime($request->branchOfficeId);

        return response()->json([
            "usersAverageServiceTime" => $usersAverageServiceTime,
        ]);
    }

    public function getUserAverageServiceTime(Request $request)
    {
        $userAverageServiceTime = $this->dataRecipientService->getUserAverageServiceTime(
            $request->userId,
            $request->visitPurposesIds,
        );

        return response()->json([
            "userAverageServiceTime" => $userAverageServiceTime,
        ]);
    }

    public function getUsersMaxServiceTime(Request $request)
    {
        $usersMaxServiceTime = $this->dataRecipientService->getUsersMaxServiceTime($request->branchOfficeId);
        return response()->json([
            "usersMaxServiceTime" => $usersMaxServiceTime,
        ]);
    }

    public function getCustomersAmountUserServed(
        Request $request
    ) {
        $customersAmountUserServed = $this->dataRecipientService->getCustomersAmountUserServed(
            $request->userId,
        );
        return response()->json([
            "customersAmountUserServed" => $customersAmountUserServed,
        ]);
    }

    public function getUserData(int $id)
    {
        $user = $this->user::with('visitPurposes', 'branchOffice', 'userStatus', 'roles')->find($id);

        if (!$user) {
            return "User not found";
        }

        return response()->json($user->toDomainEntity()->jsonSerialize());
    }

    public function updateStatisticsData(Request $request)
    {
        $usersAverageServiceTime = $this->dataRecipientService->getUsersAverageServiceTime($request->branchOfficeId);
        $userAverageServiceTime = $this->dataRecipientService->getUserAverageServiceTime(
            $request->userId,
            $request->visitPurposesIds,
        );
        $customersAmountUserServed = $this->dataRecipientService->getCustomersAmountUserServed(
            $request->userId
        );
        $usersMaxServiceTime = $this->dataRecipientService->getUsersMaxServiceTime($request->branchOfficeId);
        event(new UserStatisticsDataEvent(
            $request->userId,
            $usersAverageServiceTime,
            $userAverageServiceTime,
            $customersAmountUserServed,
            $usersMaxServiceTime,
        ));
    }

    public function updateUserStatus(Request $request)
    {
        $useCaseRequest = new UpdateUserStatusRequest(
            $request->userId,
            $request->statusId
        );

        $useCase = app(UpdateUserStatusUseCase::class);
        $isUpdated = $useCase->perform($useCaseRequest);
        $userData = $this->userRepository->getUserDataFilteredById($request->userId);

        return response()->json([
            "message" => $isUpdated ? "STATUS_IS_UPDATED" : "STATUS_IS_NOT_UPDATED",
            "userData" => $userData
        ]);
    }
}
