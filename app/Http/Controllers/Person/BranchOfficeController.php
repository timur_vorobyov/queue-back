<?php

namespace App\Http\Controllers\Person;

use App\Http\Controllers\Controller;
use App\Modules\BranchOffice\Person\Requests\ShowBranchOfficeRequest;
use App\Modules\BranchOffice\Person\UseCases\ShowBranchOfficesUseCase;

class BranchOfficeController extends Controller
{
    public function showBranchOffices(int $townId)
    {
        $useCaseRequest = new ShowBranchOfficeRequest($townId);

        $useCase = app(ShowBranchOfficesUseCase::class);
        $branchOffices = $useCase->perform($useCaseRequest);

        return response()->json($branchOffices);
    }
}
