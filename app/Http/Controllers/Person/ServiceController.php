<?php

namespace App\Http\Controllers\Person;

use App\Modules\Service\Person\Interfaces\IServiceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController
{
    private Validator $validator;
    private IServiceRepository $serviceRepository;

    public function __construct()
    {
        $this->validator = app(Validator::class);
        $this->serviceRepository = app(IServiceRepository::class);
    }

    public function showServices(Request $request)
    {
        $this->validator::make($request->all(), [
            "visitPurposeId" => "required",
        ]);

        $service = $this->serviceRepository->getServiceListFilteredByVisitPurposeId($request->visitPurposeId);

        return response()->json($service);
    }
}
