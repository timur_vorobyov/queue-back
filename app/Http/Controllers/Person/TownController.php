<?php

namespace App\Http\Controllers\Person;

use App\Http\Controllers\Controller;
use App\Modules\Town\Person\UseCases\ShowTownsUseCase;

class TownController extends Controller
{
    public function showTowns()
    {
        $useCase = app(ShowTownsUseCase::class);
        $towns = $useCase->perform();

        return response()->json($towns);
    }
}
