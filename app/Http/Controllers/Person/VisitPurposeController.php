<?php

namespace App\Http\Controllers\Person;

use App\Http\Controllers\Controller;
use App\Modules\VisitPurpose\Person\Requests\ShowVisitPurposeRequest;
use App\Modules\VisitPurpose\Person\UseCases\ShowVisitPurposeUseCase;

class VisitPurposeController extends Controller
{
    public function showVisitPurposes(int $branchOfficeId)
    {
        $useCaseRequest = new ShowVisitPurposeRequest($branchOfficeId);

        $useCase = app(ShowVisitPurposeUseCase::class);
        $visitPurposes = $useCase->perform($useCaseRequest);

        return response()->json($visitPurposes);
    }
}
