<?php


namespace App\Http\Controllers;


use App\Modules\BranchOffice\BranchOffice;
use App\Modules\User\User;
use App\Modules\UserStatus\UserStatus;
use App\Modules\VisitPurpose\VisitPurpose;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

class ModulesDataListMediator extends Controller
{
    public function getUsersList()
    {
        return User::with('visitPurposes', 'branchOffice', 'roles', "userStatus")->get()->map(function ($user) {
            return $user->toDomainEntity()->jsonSerialize();
        });
    }

    public function getBranchOfficesList()
    {
        return BranchOffice::with('visitPurposes', 'town')->get()->map(function ($branchOffice) {
            return $branchOffice->toDomainEntity()->jsonSerialize();
        });
    }

    public function getVisitPurposesList()
    {
        return VisitPurpose::all()->map(function ($visitPurpose) {
            return $visitPurpose->toDomainEntity()->jsonSerialize();
        });
    }

    public function getUserStatusesList()
    {
        return UserStatus::all()->toArray();
    }

    /**
     * @return JsonResponse
     */
    public function getVisitPurposes(): JsonResponse
    {
        return response()->json(VisitPurpose::all());
    }

}
