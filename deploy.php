<?php
namespace Deployer;


require 'recipe/laravel.php';
require 'recipe/rsync.php';

set('application', 'queue.backend');
set('repository','git@github.com:alifcapital/queue.backend.git');
set('composer_action','install --ignore-platform-reqs');
set('keep_releases', 1);
set('ssh_multiplexing', true);
set('rsync_src', function () {
    return __DIR__;
});


add('rsync', [
    'exclude' => [
        '/.git',
        '/.env',
        '/storage/',
        '/vendor/',
        '/node_modules/',
        '/.github',
        '/deploy.php',
        '/laravel-echo-server.json'
    ],
]);

task('deploy:secrets', function () {
    file_put_contents(__DIR__ . '/.env', getenv('DOT_ENV'));
    upload('.env', get('deploy_path') . '/shared');
});

host('nq.alif.tj')
    ->hostname('192.168.13.178')
    ->stage('staging')
    ->roles('app')
    ->user('timur')
    ->set('branch', 'develop')
    ->set('deploy_path', '/var/www/queue.backend');

host('apinq.alif.tj')
    ->hostname('192.168.15.133')
    ->stage('production')
    ->roles('app')
    ->user('timur')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/queue.backend');

after('deploy:failed', 'deploy:unlock');

desc('Deploy the application');

task('deploy staging', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:secrets',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);
